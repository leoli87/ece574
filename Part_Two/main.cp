//-----------------------------------------------------------*/
//  Function: CALD Lab2
//  Date: 2014/10/13
//	Author: kspeng, aoli1
//-----------------------------------------------------------*/

#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <stdlib.h>
#include <stdio.h>
#include <cstddef>
#include <vector>
#include <list>
#include <map>


// Declarations
using namespace std;

fstream netlistFile;   //define two objections.  netlistFile is the source;verilogFile object.
fstream verilogFile;

//- System
int iDbg = 0;               // 0: off, 1: Level1, 2: Level2
bool bErrorFlag = 0;

//- Component Type
enum VarType{ input, output, wire, reg, NotSupport};
enum VarFormat{ Int1, Int8, Int16, Int32, Int64, UInt1, UInt8, UInt16, UInt32, UInt64, Invalid};
enum CompType{ REG, ADD, SUB, COMPGT, COMPLT, COMPEQ, MUX2x1, SHR, SHL, MUL, DIV, MOD, NotDefine};
//- Component Array
string strFormat[11] = { "#(1)", "#(8)", "#(16)", "#(32)", "#(64)", "#(1)", "#(8)", "#(16)", "#(32)", "#(64)", "#Invalid" };
string strType[13] = { "REG", "ADD", "SUB", "COMPGT", "COMPLT", "COMPEQ", "MUX2x1", "SHR", "SHL", "MUL", "DIV", "MOD", "NotDefine"};
string InandOut[36] = { "q", "d", "sum", "a", "b", "diff", "a", "b", "gt", "a", "b", "lt", "a", "b", "eq", "a", "b", "d", "sel", "a", "b", "d", "sh_amt", "a", "d", "sh_amt", "a", "prod", "a", "b", "quot", "a", "b", "rem", "a", "b" };
int format[11] = { 1, 8, 16, 32, 64, 1, 8, 16, 32, 64,Invalid};
string varf[11] = { "Int1", "Int8", "Int16", "Int32", "Int64", "UInt1", "UInt8", "UInt16", "UInt32", "UInt64", "Invalid" };
//- Delay Table [CompType][VarFormat]
double dbDelayTable[12][10] = {
    {0.754, 0.776,  0.8,    0.825,  0.828,   0.754, 0.776,  0.8,    0.825,  0.828},
    {0.718,	1.178,	1.293,	1.523,	1.983,   0.718,	1.178,	1.293,	1.523,	1.983},
    {0.718,	1.584,	1.749,	1.939,	2.46,    0.718,	1.584,	1.749,	1.939,	2.46},
    {0.712,	1.997,	1.572,	1.684,	1.908,   0.712,	1.997,	1.572,	1.684,	1.908},
    {0.712,	1.901,	1.529,	1.641,	1.865,   0.712,	1.901,	1.529,	1.641,	1.865},
    {0.712,	1.024,	1.336,	1.408,	1.562,   0.712,	1.024,	1.336,	1.408,	1.562},
    {0.768,	0.792,	0.819,	0.848,	0.852,   0.768,	0.792,	0.819,	0.848,	0.852},
    {0.712,	1.211,	1.44,	1.701,	1.705,   0.712,	1.211,	1.44,	1.701,	1.705},
    {0.712,	1.223,	1.374,	1.696,	1.674,   0.712,	1.223,	1.374,	1.696,	1.674},
    {0.689,	3.131,	3.362,	5.717,	8.169,   0.689,	3.131,	3.362,	5.717,	8.169},
    {0.693,	9.713,	22.916,	52.048,	115.463, 0.693,	9.713,	22.916,	52.048,	115.463},
    {0.928,	10.565,	23.337,	52.461,	115.939, 0.928,	10.565,	23.337,	52.461,	115.939}};

//- Global Index
int iCompNum = 0;

//- Data Parser Info
vector<int> iFileInfo;
string strFileArr[1024][1024];
int iStrLineNum, iVarNum;
int iVerticeNum = 0;
fstream strFile;
int c=0;
int z = 0;
int i, j;
string strName;

// Critical Path Value
double dbCrtPath;


//- Function Alias Declaration
void fnCopyFile();
int instrFileArr();
int fnDataParser();//(string strFileName);
int fnDataClass();
double fnCriticalPath();
void detect();
void fnDataWrite();
void CopOut();


// Class ----------------------------------------------------//
//-----------------------------------------------------------//
//  Class Definition: clsVar
//  Target: Set Variable Class
//  Member:
//          Name: strName
//          Format: strFormat
//          Type: strType
//-----------------------------------------------------------//
class clsVar
{
public:
    string strName;         // ex: a
    string strFormat;       // ex: Int8
    string strType;     // ex: Input
    int iSrcCompNum;
    
    string getName(){
        return strName;
    }
    
    string getFormat(){
        return strFormat;
    }
    
    string getType(){
        return strType;
    }
    
    int getSrcCompNum(){
        return iSrcCompNum;
    }
    
    int setSrcCompNum(int iSrcCompNum){
        this->iSrcCompNum = iSrcCompNum;
        return iSrcCompNum;
    }
    
    clsVar() {}
    
    clsVar(string strType, string strFormat, string strName, int iSrcCompNum){
        this->strName = strName;
        this->strFormat = strFormat;
        this->strType = strType;
        this->iSrcCompNum = iSrcCompNum;
    }
};

//-----------------------------------------------------------//
//  Class Definition: clsComp
//  Target: Set Component Class
//  Member:
//          Label: iVerticeNum
//          Format: strFormat
//          Type: strType
//          Delay: dbDelay
//          Input Number: iInputNum
//          Input: strInput
//          Output: strOutput
//          Predecessor: strPred
//          Successor: strSucc
//-----------------------------------------------------------//
class clsComp
{
public:
    int iVerticeNum;
    CompType strType;
    VarFormat strFormat;
    double dbDelay;
    vector<int> iPreNum;    //  Predecessor Vertex Number
    vector<int> iSucNum;            //  Successor Vertex Number
    int iPreSize;
    
    clsComp();
    
    int getVerticeNum(){
        return iVerticeNum;
    }
    
    VarFormat getFormat(){
        return strFormat;
    }
    
    CompType getType(){
        return strType;
    }
    
    double getDelay(){
        return dbDelay;
    }
    
    vector<int> getPreNum(){
        return iPreNum;
    }
    
    vector<int>  getSucNum(){
        return iSucNum;
    }
    
    vector<int> setSucNum(int iSucNum1){
        iSucNum.push_back (iSucNum1);
        return iSucNum;
    }
    
    // 1 Input
    clsComp(int iVerticeNum, CompType strType, VarFormat strFormat, int iPreNum1){
        iPreSize = 1;
        iPreNum.push_back (iPreNum1);
        fnCompInfo(iVerticeNum, strType, strFormat);
    }
    // 2 Input
    clsComp(int iVerticeNum, CompType strType, VarFormat strFormat, int iPreNum1, int iPreNum2){
        iPreSize = 2;
        iPreNum.push_back (iPreNum1);
        iPreNum.push_back (iPreNum2);
        fnCompInfo(iVerticeNum, strType, strFormat);
    }
    // 3 Input
    clsComp(int iVerticeNum, CompType strType, VarFormat strFormat, int iPreNum1, int iPreNum2, int iPreNum3){
        iPreSize = 3;
        iPreNum.push_back (iPreNum1);
        iPreNum.push_back (iPreNum2);
        iPreNum.push_back (iPreNum3);
        fnCompInfo(iVerticeNum, strType, strFormat);
    }
    
    int fnCompInfo(int iVerticeNum, CompType strType, VarFormat strFormat){
        this->strType = strType;
        this->strFormat = strFormat;
        this->iVerticeNum = iVerticeNum;
        this->dbDelay = dbDelayTable[strType][strFormat];
        // Update Output Variable Source Vertex to be Cuurent Vertex
        if (iDbg>0){
            cout << "Vertex Num = " << iVerticeNum << endl;
            cout << "CompType = " << strType << " VarFormat = " << strFormat << endl;
            cout << "compdelay = " << dbDelay << endl;
        }
        return 0;
    }
    
};

// map ------------------------------------------------------//
map<string, clsVar> mapVar;
map<int, clsComp> mapComp;
map<int, clsComp>::iterator it;
// Sub Function ---------------------------------------------//
//-----------------------------------------------------------//
//  Sub Function Name: fnDataClass
//-----------------------------------------------------------//
CompType fnCompTypeEnum(string strType);

//-----------------------------------------------------------//
//  Sub Function Name: fnDataParser
//-----------------------------------------------------------//
VarFormat fnVarFormatEnum(string strFormat);

//-----------------------------------------------------------//
//  Sub Function Name: fnVectorMax
//-----------------------------------------------------------//
double fnVectorMax(vector<double> dbSrc);

//-----------------------------------------------------------//
//  Sub Function Name: fnVarTypeEnum
//  Target: Convert Variable Type from String to Enum
//  Input: Input Variable Tyep in String format
//  Output: Input Variable Tyep in Enum format
//-----------------------------------------------------------//
VarType fnVarTypeEnum(string strType){
    VarType varType_e;
    if (strType == "input")
        varType_e = input;      // 0
    else if (strType == "output")
        varType_e = output;     // 1
    else if (strType == "wire")
        varType_e = wire;       // 2
    else if ((strType == "register") || (strType == "reg"))
        varType_e = reg;        // 3
    else
        varType_e = NotSupport; // 4
    return varType_e;
}

//-----------------------------------------------------------//
//  Sub Function Name: fnCompTypeEnum
//  Target: Convert Component Type from String to Enum
//  Input: Input Component Tyep in String format
//  Output: Input Component Tyep in Enum format
//-----------------------------------------------------------//
CompType fnCompTypeEnum(string strType){
    CompType compType_e;
    if (strType == "+")
        compType_e = ADD;        // 1
    else if (strType == "-")
        compType_e = SUB;        // 2
    else if (strType == "*")
        compType_e = MUL;        // 9
    else if (strType == "/")
        compType_e = DIV;        // 10
    else if (strType == "%")
        compType_e = MOD;        // 11
    else if (strType == ">")
        compType_e = COMPGT;     // 3
    else if (strType == "==")
        compType_e = COMPEQ;     // 5
    else if (strType == "<")
        compType_e = COMPLT;     // 4
    else if (strType == ">>")
        compType_e = SHR;        // 7
    else if (strType == "<<")
        compType_e = SHL;        // 8
    else if ((strType == "?") || ((strType == ":")))
        compType_e = MUX2x1;        // 6
    else if (strType == "=")
        compType_e = REG;        // 0
    else
        compType_e = NotDefine;    // 12
    return compType_e;
}

//-----------------------------------------------------------//
//  Sub Function Name: fnVarFormatEnum
//  Target: Convert Variable Type from String to Enum
//  Input: Input Variable Tyep in String format
//  Output: Input Variable Tyep in Enum format
//-----------------------------------------------------------//
VarFormat fnVarFormatEnum(string strFormat){
    VarFormat verFormat_e;
    if (strFormat == "Int1")
        verFormat_e = Int1;     // 0
    else if (strFormat == "UInt1")
        verFormat_e = UInt1;    // 5
    else if (strFormat == "Int8")
        verFormat_e = Int8;     // 1
    else if (strFormat == "UInt8")
        verFormat_e = UInt8;    // 6
    else if (strFormat == "Int16")
        verFormat_e = Int16;    // 2
    else if (strFormat == "UInt16")
        verFormat_e = UInt32;   // 7
    else if (strFormat == "Int32")
        verFormat_e = Int32;    // 3
    else if (strFormat == "UInt32")
        verFormat_e = UInt64;   // 8
    else if (strFormat == "Int64")
        verFormat_e = Int64;    // 4
    else if (strFormat == "UInt64")
        verFormat_e = UInt64;   // 9
    else
        verFormat_e = Invalid;  // 10
    return verFormat_e;
}

// Main ---------------------------------------------------------- //

int main(int argc, char *argv[])
{
	/*// Get Input File Name
	string strFileName = "474a_circuit1.txt";*/
	if (argc != 3) {
		verilogFile << "Usage: dpgen netlistFile verilogFile" << endl;
		return -1;
	}
	strName = argv[1];
    if (strName.string::find(".") != -1) strName=strName.string::substr(0,strName.string::find("."));
    
	netlistFile.open(argv[1], ios::in | ios::binary);    //Open the file as "READ"
    if(!netlistFile)
    {
        cout<<"cannot open the file."<<endl;
        return 0;
    }
    fnCopyFile();
    netlistFile.close();
    netlistFile.open(argv[1], ios::in | ios::binary);    //Open the file as "READ"
	verilogFile.open(argv[2], ios::out | ios::binary);   //Open the file as "WRITE"
    

    
	//initial strFileArr
	instrFileArr();
    // Data Parsing
	fnDataParser();//(strFileName);
    // Classification
    fnDataClass();
    // Verilog Output
	//Detect error
	detect();
	// Data Write
	fnDataWrite();
	netlistFile.close();
	verilogFile.close();
    // Critical Path Output
    fnCriticalPath();
    return 0;
}
// Sub Function ------------------------------------------------------------------------------------------//

//-----------------------------------------------------------//
//  Sub Function Name: fnCriticalPath
//  Target: Find Critical Path
//  Input: mapComp
//  Output:
//          Critical Path Value
//-----------------------------------------------------------//
double fnCriticalPath(){
    int i, j, jj;
    vector<int> iPred;
    int iPredVerNumSum = 0;
    int iPredVerEnd = 0;
    double dbCurVerLP = 0;  // Current Vertiex Longest Path
    double dbCurVerDl;      // Current Vertiex Delay
    double dbPredLP;        // Pred Longest Path
    vector<double> dbLPvctr;    // Longest Path Vector
    double **iDSArr = new double*[iVerticeNum+1];       // Dijkstra Sorting Array
    if (iDbg) cout << endl << "fnCriticalPath ----------------------------------------------------- "<<endl;
    if (bErrorFlag) return 0;;
    
// Preface
    for (i = 0; i <= iVerticeNum; ++i){
        iDSArr[i] = new double[iVerticeNum+1];
        for (j = 1; j <= iVerticeNum; j++){
            if (i==0)
                iDSArr[i][j] = 0;       // set 0 row to be Critical Path Check Flag row
            else
                iDSArr[i][j] = -1;
        }}
// Dijkstra Sorting
    // Initial Source Points
    for (j = 1; j <= iVerticeNum; j++){
        iPred = mapComp.find(j)->second.getPreNum();
        iPredVerNumSum=0;
        for (jj = 0; jj < iPred.size(); jj++)
            iPredVerNumSum += iPred[jj];
        if(iPredVerNumSum==0){
            iDSArr[1][j]= mapComp.find(j)->second.getDelay();
            iDSArr[0][j] = 1;       // Set Checked Flag
        }
    }
    // Second Round Searching
    bool bLoopGo=1;
    // i : cnt loop
    i = 1;  // Start from 1st row
    while (bLoopGo){
        bLoopGo = 0;                                        // Assume Loop will be ended
        i++;
        for (j = 1; j<= iVerticeNum; j++ ){                 // j : Vertex loop
            // Reg Check: if Reg, Push to Longest Path Array, and Set New Longest Path = Reg Delay
            if ((mapComp.find(j)->second.getType() == 0) & (iDSArr[i-1][j]>0) & (j != iVerticeNum)){
                dbLPvctr.push_back(iDSArr[i-1][j]);
                iDSArr[i-1][j] = mapComp.find(j)->second.getDelay();;
            }
            //
            if ((iDSArr[i-1][j]>0) | (iDSArr[0][j]==1))     // if Current Vertex is Checked
                iDSArr[i][j] = iDSArr[i-1][j];              // Get Current Longest Path = Pred
            else{
                iPred = mapComp.find(j)->second.getPreNum();
                dbCurVerDl = mapComp.find(j)->second.getDelay();    // Get Current Vertex Delaty
                dbCurVerLP = 0;                             // Initialize Current Longet Path
                iDSArr[0][j] = 1;                           // Assume Checked Flag = 1
                for (jj = 0; jj < iPred.size(); jj++){      // jj : Pred loop
                    if(iPred[jj]==0){                       // if Pred = 0, do nothing
                    }else if(iDSArr[i-1][iPred[jj]]>=0){    // if Pred > 0, calculate Longest Path
                        dbPredLP = iDSArr[i-1][iPred[jj]];  // Get Pred Longest Path
                        dbCurVerLP = (dbCurVerDl + dbPredLP) > dbCurVerLP ? (dbCurVerDl + dbPredLP): dbCurVerLP;
                        iDSArr[i][j] = dbCurVerLP;          // Current Longest Path Update
                    }else{                                  // if Pred < 0, reset
                        iDSArr[i][j] = -1;                  // Reset Current Value
                        iDSArr[0][j] = 0;                   // Reset Checked Flag
                        break;                              // Break loop
                    }
                }
                bLoopGo = 1;                                // Reset to Continue loop
            }
        }
        if (i == (iVerticeNum)) bLoopGo = 0;
    }
    iPredVerEnd = i;
    for (j = 1; j <= iVerticeNum ; j++)
            dbLPvctr.push_back(iDSArr[i][j]);
    
    dbCrtPath = fnVectorMax(dbLPvctr);
    if (iDbg){
        cout << "Dijkstra Algorithm: " << endl;
        if (iDbg>1){
            cout << "< Checked Flag Vetor >" << endl;
            for (j=1;j<=iVerticeNum;j++)
                cout << iDSArr[0][j] << ", " ;
            cout << endl;
        }
        cout << "< Sorting Table > " << endl;
        for (i=1; i<=iPredVerEnd;i++){
            for (j=1;j<=iVerticeNum;j++)
                cout << iDSArr[i][j] << ", " ;
            cout << endl;}
        cout << "< Longest Path Array > " << endl;
        for (j = 0; j < dbLPvctr.size() ; j++)
            cout << dbLPvctr[j] << ",";
        cout << endl << endl;
    }
    cout << "Critical Path = " << dbCrtPath << endl;
    return dbCrtPath;
};


//-----------------------------------------------------------//
//  Sub Function Name: fnDataWrite()
//  Target: Write Data to the verilogFile.txt
//  Input: Input form mapVar and mapComp
//  Output:
//          Data Info>>
//          Data Array>>			        
//-----------------------------------------------------------//
void fnDataWrite() {
	int a, b;
	//int i, j, a,b;
	a = 0;
	b = 0; 
	//write module();
	verilogFile << "`timescale 1ns / 1ps" << endl;
	verilogFile << "module" <<" m_"<< strName << "(clk, rst," ;
	for (i = 1; i <= iFileInfo[0]; i++){
		if ((strFileArr[i][1] == "input") || (strFileArr[i][1] == "output")){
			for (j = 1; j <= 1024; j++){
				if (strFileArr[i][j] != "0"){
					if ((strFileArr[i][j] != "input") && (strFileArr[i][j] != "output")&&( strFileArr[i][j] != "reg") &&( strFileArr[i][j] != "register")){
						if ((strFileArr[i][j] != "Int1" )&& (strFileArr[i][j] != "Int8")&&(strFileArr[i][j] != "Int16") && (strFileArr[i][j] != "Int32") && (strFileArr[i][j] != "Int64")&&(strFileArr[i][j] != "UInt1") && (strFileArr[i][j] != "UInt8")&&(strFileArr[i][j] != "UInt16" )&& (strFileArr[i][j] != "UInt32") && (strFileArr[i][j] != "UInt64")){
							a = a + 1;
						}
					}
				}
			}
		}
	}
	for (i = 1; i <= iFileInfo[0]; i++){
		if ((strFileArr[i][1] == "input") || (strFileArr[i][1] == "output")){
			for (j = 1; j <= 1024; j++){
				if ((strFileArr[i][j] != "input") && (strFileArr[i][j] != "output")&& (strFileArr[i][j] != "reg") && (strFileArr[i][j] != "register")){
					if (strFileArr[i][j] != "Int1" && strFileArr[i][j] != "Int8"&&strFileArr[i][j] != "Int16" && strFileArr[i][j] != "Int32" && strFileArr[i][j] != "Int64"&&strFileArr[i][j] != "UInt1" && strFileArr[i][j] != "UInt8"&&strFileArr[i][j] != "UInt16" && strFileArr[i][j] != "UInt32" && strFileArr[i][j] != "UInt64"){
						if (strFileArr[i][j] != "0"){
							verilogFile <<" "<< strFileArr[i][j];
						}
						if (strFileArr[i][j] != "0"){
							if (b < (a - 1)){
								verilogFile << ",";
								b = b + 1;
							}
						}
					}
				}
			}
		}
			}
	
	verilogFile << ")" << ";"<<endl;
	//write input,output,wire,reg
	verilogFile << "input clk, rst;" << endl;
	for (i = 1; i <= iFileInfo[0]; i++){
		if (strFileArr[i][1] == "input" || strFileArr[i][1] == "output" || strFileArr[i][1] == "wire" || strFileArr[i][1] == "reg" || strFileArr[i][1] == "register"){
			for (j = 1; j <= 1024; j++){
				if (strFileArr[i][j] != "0"){
					if (j == 1){
						if (strFileArr[i][1] == "register")
						{
							verilogFile << "wire ";
						}
						else
						{
							verilogFile << strFileArr[i][j] << " ";
						}
					}
					else if (j == 2)
					{
						if (strFileArr[i][j] == "Int1")
						{
							verilogFile << "signed" << " " << "[" << "0" << ":" << "0" << "]" << " ";
						}
						else if (strFileArr[i][j] == "Int8")
						{
							verilogFile << "signed" << " " << "[" << "7" << ":" << "0" << "]" << " ";
						}
						else if (strFileArr[i][j] == "Int16")
						{
							verilogFile << "signed" << " " << "[" << "15" << ":" << "0" << "]" << " ";
						}
						else if (strFileArr[i][j] == "Int32")
						{
							verilogFile << "signed" << " " << "[" << "31" << ":" << "0" << "]" << " ";
						}
						else if (strFileArr[i][j] == "Int64")
						{
							verilogFile << "signed" << " " << "[" << "63" << ":" << "0" << "]" << " ";
						}
						else if (strFileArr[i][j] == "UInt1")
						{
							verilogFile << "unsigned" << " " << "[" << "0" << ":" << "0" << "]" << " ";
						}
						else if (strFileArr[i][j] == "UInt8")
						{
							verilogFile << "unsigned" << " " << "[" << "7" << ":" << "0" << "]" << " ";
						}
						else if (strFileArr[i][j] == "UInt16")
						{
							verilogFile << "unsigned" << " " << "[" << "15" << ":" << "0" << "]" << " ";
						}
						else if (strFileArr[i][j] == "UInt32")
						{
							verilogFile << "unsigned" << " " << "[" << "31" << ":" << "0" << "]" << " ";
						}
						else if (strFileArr[i][j] == "UInt64")
						{
							verilogFile << "unsigned" << " " << "[" << "63" << ":" << "0" << "]" << " ";
						}
					}
					else 
					{
						verilogFile << strFileArr[i][j];
						if (strFileArr[i][j + 1] != "0")
						{
							verilogFile << ", ";
						}
					}
				}
			}
			verilogFile << ";" << endl;
		}
	}
	//write components
	for (it = mapComp.begin(); it != mapComp.end(); ++it)
	{   
	    switch (it->second.getType()){
		case 0:
			c = 0;
			CopOut();
			break;
		case 1:
			c = 2;
			CopOut();
			break;
		case 2:
			c = 5;
			CopOut();
			break;
		case 3:
			c = 8;
			CopOut();
			break;
		case 4:
			c = 11;
			CopOut();
			break;
		case 5:
			c = 14;
			CopOut();
			break;
		case 6:
			c = 17;
			CopOut();
			break;
		case 7:
			c = 21;
			CopOut();
			break;
		case 8:
			c = 24;
			CopOut();
			break;
		case 9:
			c = 27;
			CopOut();
			break;
		case 10:
			c = 30;
			CopOut();
			break;
		case 11:
			c = 33;
			CopOut();
			break;
		case 12:
			CopOut();
			break;
		}
	}
	verilogFile << "endmodule" << endl;
}

//-----------------------------------------------------------//
//  Sub Function Name: CopOut()
//  Target: put components to Verilog file                   
//-----------------------------------------------------------//
int cont = 1;
void CopOut(){
if (it->second.getFormat() <= 4){
			if ((it->second.getType() == 3) || (it->second.getType() == 4) || (it->second.getType() == 7) || (it->second.getType() == 10)){
				verilogFile << "S" << strType[it->second.getType()] << " ";
			}
			else
			{
				verilogFile << strType[it->second.getType()] << " ";

			}
		}
else{
	if (it->second.getType() == 0){
		verilogFile << "REG" << " ";
	}
	else{
		verilogFile << strType[it->second.getType()] << " ";
	}
}
verilogFile << strFormat[it->second.getFormat()] << " ";
if (it->second.getType() == 0){
	verilogFile << "REG" << z << " ";
	z++;
	verilogFile << "(.clk(clk), .rst(rst), ";
}
else{
	verilogFile << strType[it->second.getType()] << z << " ";
	z = z + 1;
	verilogFile << "(";
}
for (cont; cont <= iFileInfo[0]; cont++){
	i = cont;
	if (strFileArr[i][1] != "input" && strFileArr[i][1] != "output" && strFileArr[i][1] != "wire" && strFileArr[i][1] != "reg" && strFileArr[i][1] != "register"){

		for (j = 1; j <= 1024; j++){
			if (strFileArr[i][j] != "0"){
				if (strFileArr[i][j] != "+" && strFileArr[i][j] != "-" && strFileArr[i][j] != "*" && strFileArr[i][j] != "/" && strFileArr[i][j] != "%" && strFileArr[i][j] != "==" && strFileArr[i][j] != "<" && strFileArr[i][j] != ">" && strFileArr[i][j] != ">>" && strFileArr[i][j] != "<<" && strFileArr[i][j] != "?"&&strFileArr[i][j] != "="&&strFileArr[i][j] != ":"){
					if ((it->second.getType() == 1) || (it->second.getType() == 2) || (it->second.getType() == 3) || (it->second.getType() == 4) || (it->second.getType() == 5) || (it->second.getType() == 9) || (it->second.getType() == 10) || (it->second.getType() == 11)){
						VarFormat formatnum = fnVarFormatEnum(mapVar.find(strFileArr[i][j])->second.getFormat());
						if (format[formatnum] < format[it->second.getFormat()])
						{
							if (varf[formatnum] == "Int1"){
								int diff = format[it->second.getFormat()] - format[formatnum];
								verilogFile << "." << InandOut[c] << "({{" << diff << "{ ~" << strFileArr[i][j] << " }}, 1})";
								c++;
								if (strFileArr[i][j + 1] != "0")
								{
									verilogFile << ", ";
								}
							}
							else{
								int diff = format[it->second.getFormat()] - format[formatnum];
								verilogFile << "." << InandOut[c] << "({{" << diff << "{" << strFileArr[i][j] << "[" << format[formatnum] << "-1]}}, " << strFileArr[i][j]<<"})";
								c++;
								if (strFileArr[i][j + 1] != "0")
								{
									verilogFile << ", ";
								}
							}
						}
						else
						{
							verilogFile << "." << InandOut[c] << "(" << strFileArr[i][j] << ")";
							c++;

							if (strFileArr[i][j + 1] != "0")
							{
								verilogFile << ", ";
							}
						}
					}
				else
				{
						verilogFile << "." << InandOut[c] << "(" << strFileArr[i][j] << ")";
						c++;

						if (strFileArr[i][j + 1] != "0")
						{
							verilogFile << ", ";
						}
				}
				}
			}
		}
		verilogFile << ")" << ";" << endl;
		break;
	}
}
cont = cont + 1;
}
//-----------------------------------------------------------//
//  Sub Function Name: instrFileArr()
//  Target: Initial strFileArr[1024][1024]                   
//-----------------------------------------------------------//
int instrFileArr(){
	for (int i = 0; i < 1024; i++){
		for (int j = 0; j < 1024; j++)
		{
			strFileArr[i][j] = "0" ;
		}
	}
	return 0;
}
//-----------------------------------------------------------//
//  Sub Function Name: detect()
//  Target: Detect error                   
//-----------------------------------------------------------//
void detect(){
	int i, j, a, b, c, d;
	a = 0;
	b = 0;
	d = 0;
	string str[1000];
	//detect input reg error
	for (i = 1; i <=iFileInfo[0]; i++){
		if (strFileArr[i][1] == "input"){
			for (j = 1; j <= 1024; j++){
				if (strFileArr[i][j] != "0"){
					if (strFileArr[i][j] == "reg" || strFileArr[i][j] == "register"){
						cout<< " Type is wrong" << endl;
						exit(0);
					}
				}
			}		
		}
	}
	//detect input variables' type are reg
	//save output
	for (i = 1; i <= iFileInfo[0]; i++){
		if (strFileArr[i][1] == "input"){
			for (j = 1; j <= 1024; j++){
				if (strFileArr[i][j] != "0"){
					if (strFileArr[i][j] != "output"){
						if (strFileArr[i][j] != "Int1" && strFileArr[i][j] != "Int8"&&strFileArr[i][j] != "Int16" && strFileArr[i][j] != "Int32" && strFileArr[i][j] != "Int64"&&strFileArr[i][j] != "UInt1" && strFileArr[i][j] != "UInt8"&&strFileArr[i][j] != "UInt16" && strFileArr[i][j] != "UInt32" && strFileArr[i][j] != "UInt64"){
							str[a] = strFileArr[i][j];
							a++;
							b++;
						}
					}
				}
			}
		}
	}
	//detect
	for (i = 1; i <= iFileInfo[0]; i++){
		if ((strFileArr[i][1] == "reg") || (strFileArr[i][1] == "reg")){
			for (j = 2; j <= 1024; j++){
				         d = 0;
				if (strFileArr[i][j] != "Int1" && strFileArr[i][j] != "Int8"&&strFileArr[i][j] != "Int16" && strFileArr[i][j] != "Int32" && strFileArr[i][j] != "Int64"&&strFileArr[i][j] != "UInt1" && strFileArr[i][j] != "UInt8"&&strFileArr[i][j] != "UInt16" && strFileArr[i][j] != "UInt32" && strFileArr[i][j] != "UInt64"){
					if (strFileArr[i][j] != "0"){
						for (c = 0; c < b; c++){
							if (strFileArr[i][j] == str[c]){
								d = 1;
							}
						}
						if (d == 1){
							cout << strFileArr[i][j] << " Type is wrong" << endl;
							exit(0);
						}				
					}
				}
			}
		}
	}
}

//-----------------------------------------------------------//
//  Sub Function Name: fnDataClass
//  Target: Parsing Data from stream to Array
//  Input: Input File Data
//  Output:
//          Data Info>>
//                  Line#cnt, Line_1_Element#cnt~Line_n_Element#cnt
//          Data Array>>
//                  Array 1, Line_1.Element1 ~ m
//                      ...
//                  Array n, Line_n.Element1 ~ m
//-----------------------------------------------------------//
int fnDataClass(){
    string strCurType;
    string strCurFormat;
    CompType curType;
    VarFormat curFormat;
    VarFormat iPreNum1Format;
    VarFormat iPreNum2Format;
    VarFormat iPreNum3Format;
    VarFormat iSucNumFormat;
    VarType iPreNum1Type;
    VarType iPreNum2Type;
    VarType iPreNum3Type;
    VarType iSucNumType;
    int iPreNum1, iPreNum2, iPreNum3;//, iSucNum;
    int iSucNum;
    int iOp1, iOp2, iOp3;
    int i, j;
    string strWr;
    
    if (iDbg) cout << endl << "fnDataClass ----------------------------------------------------- "<<endl;
    if (bErrorFlag) return 0;

    for (i=1;i<=iStrLineNum;i++){
        iVarNum = iFileInfo[i];
        // Set Variable Into Variable Map Class by Type, Format, Name
        if ((strFileArr[i][1]=="input") || (strFileArr[i][1]=="output") || (strFileArr[i][1]=="wire") || (strFileArr[i][1]=="register")  || (strFileArr[i][1]=="reg")){
            for (j=3; j<=iVarNum; j++){
                mapVar.insert (pair<string, clsVar>(strFileArr[i][j], clsVar(strFileArr[i][1], strFileArr[i][2], strFileArr[i][j], 0)));
                if (iDbg>1){
                    cout << "[" << i << ", " << j << "], " << "strFileArr[i][j]: " << strFileArr[i][j] << endl;
                    cout << mapVar.find(strFileArr[i][j])->second.getType() << " ";
                    cout << mapVar.find(strFileArr[i][j])->second.getFormat() << " ";
                    cout << mapVar.find(strFileArr[i][j])->second.getName()<< endl;
                }
            }
        }
        // Set Operant Into Compenent Map Class by VericNumber, Type, Format
        else{
            iVerticeNum++;
            // Output Variable Data Format Get
            strCurFormat = mapVar.find(strFileArr[i][1])->second.getFormat();
            curFormat = fnVarFormatEnum(strCurFormat);
            if (curFormat == 10){
                bErrorFlag = 1;
                cout << "Error: Variable '" << strFileArr[i][1] << "' is not defined." << endl;
                return 0;
            }
            // Get Element Enum #
            iPreNum1 = mapVar.find(strFileArr[i][3])->second.getSrcCompNum();
            iOp1 = fnCompTypeEnum(strFileArr[i][2]);
            iSucNum = mapVar.find(strFileArr[i][1])->second.getSrcCompNum();
            iPreNum1Format = fnVarFormatEnum(mapVar.find(strFileArr[i][3])->second.getFormat());
            iPreNum1Type = fnVarTypeEnum(mapVar.find(strFileArr[i][3])->second.getType());
            iSucNumType = fnVarTypeEnum(mapVar.find(strFileArr[i][1])->second.getType());
            iSucNumFormat = fnVarFormatEnum(mapVar.find(strFileArr[i][1])->second.getFormat());
            // Operant Mapped into Component Map Class
            switch (iVarNum){
                case 3: // Register Case
                    strCurType = strFileArr[i][2];
                    curType = fnCompTypeEnum(strFileArr[i][2]);
                    // Error Check
                    if ((iPreNum1Format == 10) || (iSucNumFormat == 10) || (iOp1 == 12)){   // Operant Format Check
                        bErrorFlag = 1;
                        cout << "Error: Operant Format is not correct." << endl;
                    }
                    if (iPreNum1Type == 1){                     //  Input Variable can't be Output
                        bErrorFlag = 1;
                        cout << "Error: Input Variable Format can not be Output." << endl;
                    }
                    if (iSucNumType == 0){                      //  Outpur Variable can't be Input
                        bErrorFlag = 1;
                        cout << "Error: Output Variable Format can not be Input." << endl;
                    }
                    if (bErrorFlag){
                        cout << "Line " << i << ": " << strFileArr[i][1] << " " << strFileArr[i][2] << " " << strFileArr[i][3]<< endl;
                        return 0;
                    }
                    // Set Pred Vertices' Succ #
                    if (iPreNum1) mapComp.find(iPreNum1)->second.setSucNum(iVerticeNum);
                    // Insert New Element into Component Class
                    mapComp.insert(pair<int, clsComp>(iVerticeNum, clsComp(iVerticeNum, curType, curFormat,iPreNum1)));
                    break;
                case 5: // Operant Case
                    strCurType = strFileArr[i][4];
                    curType = fnCompTypeEnum(strFileArr[i][4]);
                    iPreNum2 = mapVar.find(strFileArr[i][5])->second.getSrcCompNum();
                    iOp2 = fnCompTypeEnum(strFileArr[i][4]);
                    iPreNum2Format = fnVarFormatEnum(mapVar.find(strFileArr[i][5])->second.getFormat());
                    iPreNum2Type = fnVarTypeEnum(mapVar.find(strFileArr[i][5])->second.getType());
                    //if ((curType >=3) & (curType <=5)) curFormat = iPreNum1Format>iPreNum2Format?iPreNum1Format:iPreNum2Format;
                    // Error Check
                    if ((iPreNum1Format == 10) || (iPreNum2Format == 10) || (iSucNumFormat == 10) || (iOp1 == 12) || (iOp2 == 12)){ // Operant Format Check
                        bErrorFlag = 1;
                        cout << "Error: Operant Format is not correct." << endl;
                    }
                    if ((iPreNum1Type == 1) || (iPreNum2Type == 1)){        //  Input Variable can't be Output
                        bErrorFlag = 1;
                        cout << "Error: Input Variable Format can not be Output." << endl;
                    }
                    if (iSucNumType == 0){                                  //  Outpur Variable can't be Input
                        bErrorFlag = 1;
                        cout << "Error: Output Variable Format can not be Input." << endl;
                    }
                    if (bErrorFlag){
                        cout << "Line " << i << ": " << strFileArr[i][1] << " " << strFileArr[i][2] << " " << strFileArr[i][3] << " " << strFileArr[i][4] << " " << strFileArr[i][5]<< endl;
                        return 0;
                    }
                    // Set Pred Vertices' Succ #
                    if (iPreNum1) mapComp.find(iPreNum1)->second.setSucNum(iVerticeNum);
                    if (iPreNum2) mapComp.find(iPreNum2)->second.setSucNum(iVerticeNum);
                    // Insert New Element into Component Class
                    // Output wire check
                    if (iSucNumType == 1){
                        strWr = strFileArr[i][1]+ "wire";
                        while (1) {
                            if (mapVar.find(strWr)->second.getName() == strWr)
                                strWr = strWr + "0";
                            else break;
                        }
                        //1. add new wire op in the bottum
                        iFileInfo[0] += 1;
                        iFileInfo.push_back(3);
                        strFileArr[iFileInfo[0]][1] = strFileArr[i][1];
                        strFileArr[iFileInfo[0]][2] = "=";
                        strFileArr[iFileInfo[0]][3] = strWr;
                        //2. add wire case in array
                        iFileInfo[0] += 1;
                        iFileInfo.push_back(3);
                        strFileArr[iFileInfo[0]][1] = "wire";
                        strFileArr[iFileInfo[0]][2] = mapVar.find(strFileArr[i][1])->second.getFormat();
                        strFileArr[iFileInfo[0]][3] = strWr;
                        mapVar.insert (pair<string, clsVar>(strFileArr[iFileInfo[0]][3], clsVar(strFileArr[iFileInfo[0]][1], strFileArr[iFileInfo[0]][2], strFileArr[iFileInfo[0]][3], iVerticeNum)));
                        //3. rename output to be " "+wire
                        strFileArr[i][1] = strWr;
                        //4. increas Line num
                        iStrLineNum ++;
                        //5. add new comp
                        mapComp.insert(pair<int, clsComp>(iVerticeNum, clsComp(iVerticeNum, curType, curFormat,iPreNum1, iPreNum2)));
                    }else{
                        mapComp.insert(pair<int, clsComp>(iVerticeNum, clsComp(iVerticeNum, curType, curFormat,iPreNum1, iPreNum2)));
                    }
                    break;
                case 7: // Compare Case
                    strCurType = strFileArr[i][4];
                    curType = fnCompTypeEnum(strFileArr[i][4]);
                    iPreNum2 = mapVar.find(strFileArr[i][5])->second.getSrcCompNum();
                    iOp2 = fnCompTypeEnum(strFileArr[i][4]);
                    iPreNum2Format = fnVarFormatEnum(mapVar.find(strFileArr[i][5])->second.getFormat());
                    iPreNum2Type = fnVarTypeEnum(mapVar.find(strFileArr[i][5])->second.getType());
                    iPreNum3 = mapVar.find(strFileArr[i][7])->second.getSrcCompNum();
                    iOp3 = fnCompTypeEnum(strFileArr[i][6]);
                    iPreNum3Format = fnVarFormatEnum(mapVar.find(strFileArr[i][7])->second.getFormat());
                    iPreNum3Type = fnVarTypeEnum(mapVar.find(strFileArr[i][7])->second.getType());
                    if ((iPreNum1Format == 10) || (iPreNum2Format == 10) || (iPreNum3Format == 10) || (iSucNumFormat == 10) || (iOp1 == 12) || (iOp2 == 12) || (iOp3 == 12)){   // Operant Format Check
                        bErrorFlag = 1;
                        cout << "Error: Operant Format is not correct." << endl;
                        return 0;
                    }
                    //  Input Variable can't be Output
                    if ((iPreNum1Type == 1) || (iPreNum2Type == 1)  || (iPreNum3Type == 1)){
                        bErrorFlag = 1;
                        cout << "Error: Input Variable Format can not be Output." << endl;
                    }
                    if (iSucNumType == 0){                                  //  Outpur Variable can't be Input
                        bErrorFlag = 1;
                        cout << "Error: Output Variable Format can not be Input." << endl;
                    }
                    if (bErrorFlag){
                        cout << "Line " << i << ": " << strFileArr[i][1] << " " << strFileArr[i][2] << " " << strFileArr[i][3] << " " << strFileArr[i][4] << " " << strFileArr[i][5] << " " << strFileArr[i][6] << " " << strFileArr[i][7]<< endl;
                        return 0;
                    }
                    
                    // Set Pred Vertices' Succ #
                    if (iPreNum1) mapComp.find(iPreNum1)->second.setSucNum(iVerticeNum);
                    if (iPreNum2) mapComp.find(iPreNum2)->second.setSucNum(iVerticeNum);
                    if (iPreNum3) mapComp.find(iPreNum3)->second.setSucNum(iVerticeNum);
                    // Insert New Element into Component Class
                    
                    // Output wire check
                    if (iSucNumType == 1){
                        strWr = strFileArr[i][1]+ "wire";
                       /* while (1) {
							if (mapVar.find(strWr)->second.getName() == strWr)
							{
								strWr = strWr + "0";
							}
							else
							{
								break;
							}
                        } */
						//----- still not work on circuit1 10/12/2014
                        //1. add new case in the bottum
                        iFileInfo[0] += 1;
                        iFileInfo.push_back(3);
                        strFileArr[iFileInfo[0]][1] = strFileArr[i][1];
                        strFileArr[iFileInfo[0]][2] = "=";
                        strFileArr[iFileInfo[0]][3] = strFileArr[i][1] + "wire";
                        //2. add wire case in array
                        iFileInfo[0] += 1;
                        iFileInfo.push_back(3);
                        strFileArr[iFileInfo[0]][1] = "wire";
                        strFileArr[iFileInfo[0]][2] = mapVar.find(strFileArr[i][1])->second.getFormat();
                        strFileArr[iFileInfo[0]][3] = strFileArr[i][1] + "wire";
                        mapVar.insert (pair<string, clsVar>(strFileArr[iFileInfo[0]][3], clsVar(strFileArr[iFileInfo[0]][1], strFileArr[iFileInfo[0]][2], strFileArr[iFileInfo[0]][3], iVerticeNum)));
                        //3. rename output to be " "+wire
                        strFileArr[i][1] = strFileArr[i][1] + "wire";
                        //4. increas Line num
                        iStrLineNum ++;
                        //5. add new comp
                        mapComp.insert(pair<int, clsComp>(iVerticeNum, clsComp(iVerticeNum, curType, curFormat,iPreNum1, iPreNum2, iPreNum3)));
                        
                    }else{
                        mapComp.insert(pair<int, clsComp>(iVerticeNum, clsComp(iVerticeNum, curType, curFormat,iPreNum1, iPreNum2, iPreNum3)));
                    }
                    break;
                default:
                    bErrorFlag = 1;
                    cout << "Error: Operant Format is not corrext." << endl;
                    return 0;
                    break;
                }
                mapVar.find(strFileArr[i][1])->second.setSrcCompNum(iVerticeNum);
        }
    }
    for (i=1;i<=iVerticeNum;i++){
    // Debug--------------------------------------------------------- //
            if (iDbg>1){
                //cout << "[" << i << ", 1], " << "strFileArr[i][1]: " << strFileArr[i][1] << endl;
                cout << "Vertex Num : " << mapComp.find(i)->second.getVerticeNum() << " " << endl;
                cout << "strCurType : " << mapComp.find(i)->second.getType() << " " << endl;
                cout << "strCurFormat : " << mapComp.find(i)->second.getFormat()<< endl;
                cout << "Delay : "<< mapComp.find(i)->second.getDelay()<< endl;
                vector<int> iPreNum = mapComp.find(i)->second.getPreNum();
                for (int j=0; j<iPreNum.size(); j++)
                    cout << "Pred Vertices Num = " << iPreNum[j] << endl;
                vector<int> iSucNum = mapComp.find(i)->second.getSucNum();
                for (int j=0; j<iSucNum.size(); j++)
                    cout << "Succ Vertices Num = " << iSucNum[j] << endl;
                cout << endl;
            }
    }
    
    // Debug--------------------------------------------------------- //
    if (iDbg){
        cout << "<< File to Array Output >>" << endl;
        int iLineNum = 0;
        while (iLineNum<iFileInfo[0])
        {
            iLineNum++;
            if (iDbg>1)
                cout << ">>iLineNum = " << iLineNum << " >>iLnElmtNum = " << iFileInfo[iLineNum] << endl;
            int iLnElmtNum = 0;
            while (iLnElmtNum<iFileInfo[iLineNum])
            {
                iLnElmtNum++;
                
                cout << strFileArr[iLineNum][iLnElmtNum] << " ";
                
            }
            cout << endl;
        }
    }
    
    return 0;
}
//-----------------------------------------------------------//
//  Sub Function Name: fnDataParser
//  Target: Parsing Data from stream to Array
//  Input: Input File Data
//  Output:
//          Data Info>>
//                  Line#cnt, Line_1_Element#cnt~Line_n_Element#cnt
//          Data Array>>
//                  Array 1, Line_1.Element1 ~ m
//                      ...
//                  Array n, Line_n.Element1 ~ m
//-----------------------------------------------------------//

int fnDataParser()//(string strFileName)
{
    // Initilization
    int iLineNum=0;
    int iLnElmtNum;
    string strLine, strLnElmt;
    // file read

    if (iDbg) cout << endl << "fnDataParser ----------------------------------------------------- "<<endl;    
    // Initialize iFileInfo to Get String Line Num
    while (!strFile.eof())
    {
        getline(strFile, strLine);
        if ((strLine.length())>0){
            iLineNum++;
        }
    }
    
    iFileInfo.push_back(18);
  
    strFile.close();
    
    // Initialize strFileArr
    iLineNum=0;
    // Get String Line Num and Relative Stream Num
    while (!netlistFile.eof())
    {
        // get line string
        getline(netlistFile, strLine);
        
        // remove "//"
        if (strLine.string::find("//") != -1)
            strLine=strLine.string::substr(0,strLine.string::find("//"));
        // replace "," by " "
		int pos;
		while ((pos = strLine.find(',')) != -1) {
			strLine.replace(pos, 1, " ");
		} //----revised by Leo 10/12/2014
        //replace(strLine.begin(),strLine.end(),',', ' ');     
        // Put File to Array
        if ((strLine.length())>0)
        {
            // Count Line Num
            iLineNum++;
            // Initial Stream Num
            iLnElmtNum=0;
            // Get string as stream
            istringstream is(strLine);
            while(is>>strLnElmt)
            {
                iLnElmtNum++;
                // Data to Array
                strFileArr[iLineNum][iLnElmtNum] = strLnElmt.c_str();
                //arr[iLineNum][iLnElmtNum] =strLnElmt.c_str();
                //arr[iLnElmtNum] = new string[strLnElmt.c_str()];
                
            }
            // Get Line Element Number to File Info Array
            iFileInfo.push_back(iLnElmtNum);
        }
    }
    iFileInfo[0] = iLineNum;
    iStrLineNum = iLineNum;
    
    if (iFileInfo[0]==0)
    {
        bErrorFlag = 1;
        cout << "Error: Empty File" << endl;
        return 0;
    }
    for (int i=1;i<=iFileInfo[0];i++){
        if (iFileInfo[i]<3){
            bErrorFlag = 1;
            cout << "Error: File Format error" << endl;
            return 0;
        }
    }

    // Debug--------------------------------------------------------- //
    if (iDbg){
        cout << "Data Parsing : \n" << "<< File Infomation >>" << endl;
        for (int idx=0;idx<=iLineNum;idx++)
            cout << iFileInfo[idx] << " ";
        cout << endl;
        cout << "<< File to Array Output >>" << endl;
        iLineNum = 0;
        while (iLineNum<iFileInfo[0])
        {
            iLineNum++;
            if (iDbg>1)
                cout << ">>iLineNum = " << iLineNum << " >>iLnElmtNum = " << iFileInfo[iLineNum] << endl;
            iLnElmtNum = 0;
            while (iLnElmtNum<iFileInfo[iLineNum])
            {
                iLnElmtNum++;

                cout << strFileArr[iLineNum][iLnElmtNum] << " ";

            }
            cout << endl;
        }
    }
    // -------------------------------------------------------------- //
    return 0;
}

//-----------------------------------------------------------//
//  Sub Function Name: fnVectorMax
//  Target: Find Max Value in an Double Vector
//  Input: Input Vector in Double
//  Output:
//          Max Value of the Input Double Vector
//-----------------------------------------------------------//
double fnVectorMax(vector<double> dbSrc){
    double dbVectorMax = dbSrc[1];
    for (int i =0; i < dbSrc.size(); i++)
        dbVectorMax = dbVectorMax > dbSrc[i] ? dbVectorMax : dbSrc[i];
    return dbVectorMax;
}

//-----------------------------------------------------------//
//  Sub Function Name: fnCopyFile
//  Target: Find Max Value in an Double Vector
//  Input: Input Vector in Double
//  Output:
//          Max Value of the Input Double Vector
//-----------------------------------------------------------//
void fnCopyFile(){
    
    strFile.open("netlistFileStr.txt",ios::out | ios::binary);
    char tmp;
    int num;                 //count
	num = 0;
    while(!netlistFile.eof())
    {
        netlistFile.get(tmp);       //get one word from source to tmp
        strFile.put(tmp);           //from tmp to object.
        num+=1;                     //count bits.
    }
}



