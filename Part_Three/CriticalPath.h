//-----------------------------------------------------------*/
//  Function: ECE574_LAB3 Data Critical Path Calculation Header file
//  File Name: CriticalPath.h
//  Edition: 201410301610
//  Date: 2014/10/30
//  Content:
//          Data Critical Path Calculation Flow Extern
//          a.) fnCriticalPath
//          b.) fnVectorMax
//-----------------------------------------------------------*/
#ifndef ECE574_LAB3_criticalpath
#define ECE574_LAB3_criticalpath

#include "defs.h"

extern double fnCriticalPath();
extern double fnVectorMax(vector<double> dbSrc);

#endif /* defined(__test__criticalpath__) */
