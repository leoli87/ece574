//-----------------------------------------------------------*/
//  Function: ECE574_LAB3 Data Parsar Header file
//  File Name: DataParser.h
//  Edition: 201411111628
//  Date: 2014/11/11
//  Content:
//          Data Parsing Flow Extern
//          a.) fnGetFile
//          b.) fnSetFileInfo
//          c.) fnDataParser
//-----------------------------------------------------------*/

#ifndef __test__dataparser__
#define __test__dataparser__

#include "defs.h"

extern void fnGetFile(int argc, char *argv[]);
extern void fnSetFileInfo(char *argv[]);
extern int fnDataParser();



#endif /* defined(__test__dataparser__) */
