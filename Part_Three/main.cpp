//-----------------------------------------------------------*/
//  Function: ECE574_LAB3 Main file
//  File Name: main.cpp
//  Edition: 201411101140
//  Date: 2014/11/10
//  Content:
//          Main flow
//  New:
//          Merge AoLi modification
//-----------------------------------------------------------*/

#include "defs.h"
#include "CriticalPath.h"
#include "DataParser.h"
#include "DataClass.h"
#include "DataOutput.h"
#include "CDFG.h"

int main(int argc, char *argv[]){
    // - Get File
    fnGetFile(argc, argv);
    // - Data Parsing
	fnDataParser();
    // - Classification
    fnDataClass();
	// - CDFG
	fnCDFG();
    // - Verilog Output
	// Detect error
	detect();
	// - Data Write
	fnDataWrite();
    // - Critical Path Output
    //fnCriticalPath();
	cFile.close();
	verilogFile.close();
    return 0;
}







