//-----------------------------------------------------------*/
//  Function: ECE574_LAB3 Data Critical Path Calculation file
//  File Name: CriticalPath.cpp
//  Edition: 201410301610
//  Date: 2014/10/31
//  Content:
//          Data Critical Path Calculation Flow
//          a.) fnCriticalPath
//          b.) fnVectorMax
//  New:
//          Reg Count reset
//-----------------------------------------------------------*/

#include "CriticalPath.h"

//double dbCrtPath;
double fnCriticalPath();
double fnVectorMax(vector<double> dbSrc);

//-----------------------------------------------------------//
//  Sub Function Name: fnCriticalPath
//  Target: Find Critical Path
//  Input: mapComp
//  Output:
//          Critical Path Value
//-----------------------------------------------------------//
double fnCriticalPath(){
    int i, j, jj;
    double dbCrtPath;
    vector<int> iPred;
    int iPredVerNumSum = 0;
    int iPredVerEnd = 0;
    double dbCurVerLP = 0;  // Current Vertiex Longest Path
    double dbCurVerDl;      // Current Vertiex Delay
    double dbPredLP;        // Pred Longest Path
    vector<double> dbLPvctr;    // Longest Path Vector
    double **iDSArr = new double*[iVerticeNum+1];       // Dijkstra Sorting Array
    if (bErrorFlag) return 0;;
    if (iDbg) cout << endl << "fnCriticalPath ----------------------------------------------------- "<<endl;
    
    
    // Preface
    for (i = 0; i <= iVerticeNum; ++i){
        iDSArr[i] = new double[iVerticeNum+1];
        for (j = 1; j <= iVerticeNum; j++){
            if (i==0)
                iDSArr[i][j] = 0;       // set 0 row to be Critical Path Check Flag row
            else
                iDSArr[i][j] = -1;
        }}
    // Dijkstra Sorting
    // Initial Source Points
    for (j = 1; j <= iVerticeNum; j++){
        iPred = mapComp.find(j)->second.getPreNum();
        iPredVerNumSum=0;
        for (jj = 0; jj < iPred.size(); jj++)
            iPredVerNumSum += iPred[jj];
        if(iPredVerNumSum==0){
            iDSArr[1][j]= mapComp.find(j)->second.getDelay();
            iDSArr[0][j] = 1;       // Set Checked Flag
        }
    }
    // Second Round Searching
    bool bLoopGo=1;
    // i : cnt loop
    i = 1;  // Start from 1st row
    while (bLoopGo){
        bLoopGo = 0;                                        // Assume Loop will be ended
        i++;
        for (j = 1; j<= iVerticeNum; j++ ){                 // j : Vertex loop
            // Reg Check: if Reg, Push to Longest Path Array, and Set New Longest Path = Reg Delay
            if ((mapComp.find(j)->second.getType() == 0) & (iDSArr[i-1][j]>0) & (j != iVerticeNum)){
                dbLPvctr.push_back(iDSArr[i-1][j]);
                iDSArr[i-1][j] = 0;
            }
            //
            if ((iDSArr[i-1][j]>0) | (iDSArr[0][j]==1))     // if Current Vertex is Checked
                iDSArr[i][j] = iDSArr[i-1][j];              // Get Current Longest Path = Pred
            else{
                iPred = mapComp.find(j)->second.getPreNum();
                dbCurVerDl = mapComp.find(j)->second.getDelay();    // Get Current Vertex Delaty
                dbCurVerLP = 0;                             // Initialize Current Longet Path
                iDSArr[0][j] = 1;                           // Assume Checked Flag = 1
                for (jj = 0; jj < iPred.size(); jj++){      // jj : Pred loop
                    if(iPred[jj]==0){                       // if Pred = 0, do nothing
                    }else if(iDSArr[i-1][iPred[jj]]>=0){    // if Pred > 0, calculate Longest Path
                        dbPredLP = iDSArr[i-1][iPred[jj]];  // Get Pred Longest Path
                        dbCurVerLP = (dbCurVerDl + dbPredLP) > dbCurVerLP ? (dbCurVerDl + dbPredLP): dbCurVerLP;
                        iDSArr[i][j] = dbCurVerLP;          // Current Longest Path Update
                    }else{                                  // if Pred < 0, reset
                        iDSArr[i][j] = -1;                  // Reset Current Value
                        iDSArr[0][j] = 0;                   // Reset Checked Flag
                        break;                              // Break loop
                    }
                }
                bLoopGo = 1;                                // Reset to Continue loop
            }
        }
        if (i == (iVerticeNum)) bLoopGo = 0;
    }
    iPredVerEnd = i;
    for (j = 1; j <= iVerticeNum ; j++)
        dbLPvctr.push_back(iDSArr[i][j]);
    
    dbCrtPath = fnVectorMax(dbLPvctr);
    if (iDbg){
        cout << "Dijkstra Algorithm: " << endl;
        if (iDbg>1){
            cout << "< Checked Flag Vetor >" << endl;
            for (j=1;j<=iVerticeNum;j++)
                cout << iDSArr[0][j] << ", " ;
            cout << endl;
        }
        cout << "< Sorting Table > " << endl;
        for (i=1; i<=iPredVerEnd;i++){
            for (j=1;j<=iVerticeNum;j++)
                cout << iDSArr[i][j] << ", " ;
            cout << endl;}
        cout << "< Longest Path Array > " << endl;
        for (j = 0; j < dbLPvctr.size() ; j++)
            cout << dbLPvctr[j] << ",";
        cout << endl << endl;
    }
    cout << "Critical Path = " << dbCrtPath << endl;
    return dbCrtPath;
};


//-----------------------------------------------------------//
//  Sub Function Name: fnVectorMax
//  Target: Find Max Value in an Double Vector
//  Input: Input Vector in Double
//  Output:
//          Max Value of the Input Double Vector
//-----------------------------------------------------------//
double fnVectorMax(vector<double> dbSrc){
    double dbVectorMax = dbSrc[1];
    for (int i =0; i < dbSrc.size(); i++)
        dbVectorMax = dbVectorMax > dbSrc[i] ? dbVectorMax : dbSrc[i];
    return dbVectorMax;
}
