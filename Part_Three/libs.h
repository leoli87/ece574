//-----------------------------------------------------------*/
//  Function: ECE574_LAB3 Libraries Include Header file
//  File Name: libs.cpp
//  Edition: 201410301610
//  Date: 2014/10/30
//  Content:
//          Included Libraries
//-----------------------------------------------------------*/

#ifndef ECE574_LAB3_def_h
#define ECE574_LAB3_def_h


#include <stdio.h>
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <stdlib.h>
#include <cstddef>
#include <vector>
#include <list>
#include <map>

using namespace std;

#endif
