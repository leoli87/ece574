//-----------------------------------------------------------*/
//  Function: ECE574_LAB3 Data Classification file
//  File Name: DataClass.cpp
//  Edition: 201410301610
//  Date: 2014/10/30
//  Content:
//          Data Classificagtion Flow
//-----------------------------------------------------------*/

#ifndef ECE574_LAB3_DataClass
#define ECE574_LAB3_DataClass

#include "defs.h"

extern int fnDataClass();

#endif /* defined(__test__DataClass__) */
