//-----------------------------------------------------------*/
//  Function: ECE574_LAB3 Data Parsar file
//  File Name: DataParser.cpp
//  Edition: 201411111543
//  Date: 2014/11/11
//  Content:
//          Data Parsing Flow
//          a.) fnGetFile
//          b.) fnSetFileInfo
//          c.) fnDataParser
//  New:
//          FDS updated
//-----------------------------------------------------------*/

#include "DataParser.h"

void fnGetFile(int argc, char *argv[]);
void fnSetFileInfo(string strFileName);
int fnDataParser();

//-----------------------------------------------------------//
//  Sub Function Name: fnDataParser
//  Target: Parsing Data from stream to Array
//  Input: Input File Data
//  Output:
//          Data Info>>
//                  Line#cnt, Line_1_Element#cnt~Line_n_Element#cnt
//          Data Array>>
//                  Array 1, Line_1.Element1 ~ m
//                      ...
//                  Array n, Line_n.Element1 ~ m
//-----------------------------------------------------------//

int fnDataParser()//(string strFileName)
{
    // Initilization
    int iLineNum=0;
    int iLnElmtNum;
    string strLine, strLnElmt;
    if (bErrorFlag) return 0;
    if (iDbg) cout << endl << "fnDataParser ----------------------------------------------------- "<<endl;
    
    // Get String Line Num and Relative Stream Num
    while (!cFile.eof()){
        // get line string
        getline(cFile, strLine);
        // remove "//"
        if (strLine.string::find("//") != -1)
            strLine=strLine.string::substr(0,strLine.string::find("//"));
        // replace "," by " "
        long pos;
        while ((pos = strLine.find(',')) != -1) {
            strLine.replace(pos, 1, " ");
        }
        // Put File to Array
        if ((strLine.length())>0)
        {
            // Count Line Num
            iLineNum++;
            // Initial Stream Num
            iLnElmtNum=0;
            // Get string as stream
            istringstream is(strLine);
            while(is>>strLnElmt){
                iLnElmtNum++;
                strFileArr[iLineNum][iLnElmtNum] = strLnElmt.c_str();
            }
        }
    }
    
    
    if (iFileInfo[0]==0)
    {
        bErrorFlag = 1;
        cout << "Error: Empty File" << endl;
        return 0;
    }
    for (int i=1;i<=iFileInfo[0];i++){
        if (iFileInfo[i]<3){
            bErrorFlag = 1;
            cout << "Error: File Format error" << endl;
            return 0;
        }
    }
    
    // Debug--------------------------------------------------------- //
    if (iDbg){
        cout << "Data Parsing : \n" << "<< File Infomation >>" << endl;
        for (int idx=0;idx<=iLineNum;idx++)
            cout << iFileInfo[idx] << " ";
        cout << endl;
        cout << "<< File to Array Output >>" << endl;
        iLineNum = 0;
        while (iLineNum<iFileInfo[0])
        {
            iLineNum++;
            if (iDbg>1)
                cout << ">>iLineNum = " << iLineNum << " >>iLnElmtNum = " << iFileInfo[iLineNum] << endl;
            iLnElmtNum = 0;
            while (iLnElmtNum<iFileInfo[iLineNum])
            {
                iLnElmtNum++;
                
                cout << strFileArr[iLineNum][iLnElmtNum] << " ";
                
            }
            cout << endl;
        }
    }
    // -------------------------------------------------------------- //
    return 0;
}
//-----------------------------------------------------------//
//  Sub Function Name: fnGetFile
//  Target: Get Input/Output File
//  Input: File Name Argument
//  Output:
//          Input File >> cFile
//          Output File >> verilogFile
//-----------------------------------------------------------//

void fnGetFile(int argc, char *argv[]){
    // Get Input File Name
    if (argc != 4) {
        verilogFile << "Usage: hlsyn cFile latency verilogFile" << endl;
        bErrorFlag = 1;
        cout << "Input Command format Error" << endl;
        return;
    }
    fnSetFileInfo(argv);
    if (bErrorFlag) return;
    cFile.open(argv[1], ios::in | ios::binary);    //Open the file as "READ"
    dbSysLat = atof(argv[2]); //type conversion char to double
    verilogFile.open(argv[3], ios::out | ios::binary);   //Open the file as "WRITE"

}

//-----------------------------------------------------------//
//  Sub Function Name: fnDataParser
//  Target: Parsing Data from stream to Array
//  Input: Input File Data
//  Output:
//          Data Info>>
//                  Line#cnt, Line_1_Element#cnt~Line_n_Element#cnt
//          Data Array>>
//                  Array 1, Line_1.Element1 ~ m
//                      ...
//                  Array n, Line_n.Element1 ~ m
//-----------------------------------------------------------//

void fnSetFileInfo(char *argv[]){
    strName = argv[1];
    if (strName.string::find(".") != -1) strName=strName.string::substr(0,strName.string::find("."));
    cFile.open(argv[1], ios::in | ios::binary);    //Open the file as "READ"
    strFile.open("tmp.txt",ios::out | ios::binary);
    char tmp;
    int num = 0;                 //count
    if (!cFile){
        cout << "no such filename!" << endl;
        bErrorFlag=1;
        return;
    }
    while(!cFile.eof())
    {
        cFile.get(tmp);       //get one word from source to tmp
        strFile.put(tmp);           //from tmp to object.
        num+=1;                     //count bits.
    }
    cFile.close();
    strFile.close();
    
    
    strFile.open("tmp.txt", ios::in | ios::binary);
    // Initilization
    int iLineNum=0;
    int iLnElmtNum;
    string strLine, strLnElmt;
    
    
    
    // Initialize strFileArr
    iLineNum=0;
    iFileInfo.push_back(iLineNum);
    // Get String Line Num and Relative Stream Num
    while (!strFile.eof())
    {
        // get line string
        getline(strFile, strLine);
        
        // remove "//"
        if (strLine.string::find("//") != -1)
            strLine=strLine.string::substr(0,strLine.string::find("//"));
        // replace "," by " "
        long pos;
        while ((pos = strLine.find(',')) != -1) {
            strLine.replace(pos, 1, " ");
        } //----revised by Leo 10/12/2014
        //replace(strLine.begin(),strLine.end(),',', ' ');
        // Put File to Array
        if ((strLine.length())>0)
        {
            // Count Line Num
            iLineNum++;
            // Initial Stream Num
            iLnElmtNum=0;
            // Get string as stream
            istringstream is(strLine);
            while(is>>strLnElmt)
                iLnElmtNum++;
            
            // Get Line Element Number to File Info Array
            iFileInfo.push_back(iLnElmtNum);
        }
    }
    iFileInfo[0] = iLineNum;
    iStrLineNum = iLineNum;
    
    //string **c;
    strFileArr = new string*[iLineNum+1];  // array of pointers, c points to first element
    
    for (unsigned int i = 0; i <= iLineNum; ++i)
        strFileArr[i] = new string[iFileInfo[i]+1];
    
    
    strFile.close();
    remove( "tmp.txt" );
}