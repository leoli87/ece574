//-----------------------------------------------------------*/
//  Function: ECE574_LAB3 Data Output Header file
//  File Name: DataOutput.h
//  Edition: 201410301610
//  Date: 2014/10/30
//  Content:
//          Data Output Flow Extern
//          a.) fnDataWrite
//          b.) detect
//          c.) CopOut
//-----------------------------------------------------------*/


#ifndef ECE574_LAB3_DataOutput
#define ECE574_LAB3_DataOutput

#include "defs.h"

extern void detect();
extern void fnDataWrite();
extern void CopOut();

#endif /* defined(__test__DataOutput__) */
