//-----------------------------------------------------------*/
//  Function: ECE574_LAB3 Control Control Datapath Flow Graph file
//  File Name: CDFG.cpp
//  Edition: 201411111539
//  Date: 2014/11/11
//  Content:
//          a.)fnCDFGInit;
//          b.)fnASAPInit;
//          c.)fnALAPInit;
//          d.)fnFDSInit; 100% Not yet optimized
//  New:
//          mapFDS Output Bug Fixed
//-----------------------------------------------------------*/
#include "CDFG.h"

map<int, clsComp> mapCDFGSrc;
map<int, clsComp> mapASAP;
map<int, clsComp> mapALAP;
map<int, clsComp> mapFDS;

// Private
CompType cmpCurType;
VarFormat cmpCurFormat;
int iCurVertexNum;
double dbCurCompLat;
vector<int> iCurCompPred;
vector<int> iCurCompSuc;
vector<int> iPreNum;
vector<int> iSucNum;
vector<int> iASAPET;
int iPreNumMax, iSucNumMin;
double dbCurLat, dbCurSt, dbCurEt, dbPreEt, dbSucSt;
double dbCurState;
int iStateNum;
//double dbSysLat = 10;
CompType compCurType;
double iTmRng;

vector<vector<int> > iTmFrm;
vector<vector<double> > dbMulDistGr;
vector<vector<double> > dbDivDistGr;
vector<vector<double> > dbAddDistGr;
vector<vector<double> > dbAluDistGr;
vector<vector<double> > dbFrcTbl;
vector<vector<double> > dbSlfFrcTbl;
vector<vector<double> > dbPreFrcTbl;
vector<vector<double> > dbSucFrcTbl;
vector<vector<double> > dbOutputTbl;

// Sub Function
void fnTblSzInit();
void fnCDFG();
void fnCDFGInit();
void fnASAPInit();
void fnALAPInit();
void fnFDSInit();
double fnSucFrcCalc(int iCurVertexNum, double dbCurState, vector<int> iCurCompSuc);
double fnPreFrcCalc(int iCurVertexNum, double dbCurState, vector<int> iCurCompPred);
double fnFrcCalc(int iCurVertexNum, CompType compCurType, int iTmFrmSt, int iTmFrmEd, double dbCurState);
void fnTmFrmInit();
void fnOpDistGrInit();
void fnFrcTblInit();

// Main CDFG
void fnCDFG(){
    if (bErrorFlag) return;
    iStateNum = (int)dbSysLat;
    fnCDFGInit();
    fnASAPInit();
    fnALAPInit();
    fnFDSInit();
}


//-----------------------------------------------------------//
//  Sub Function Name: fnFDSInit
//  Target: Force Directed Scheduling Initialization
//  Input:  mapASAP mapALAP
//  Output:
//          mapFDS
//-----------------------------------------------------------//

void fnFDSInit(){
    if (bErrorFlag) return;
    double dbSlfFrc, dbPreFrc, dbSucFrc;
    int iTmFrmSt, iTmFrmEd;
    int iVertexCnt = 0;
    int iVertexEnergy = 1;
    
    // Table Size Initial
    fnTblSzInit();
    // Time Frame Init
    fnTmFrmInit();
    if (iDbg) {
        cout << "<<Time Frame>>" << endl;
        for (iCurVertexNum = 1; iCurVertexNum <= iVerticeNum; iCurVertexNum++)
            cout << "[" << iTmFrm[iCurVertexNum][0] << ", " << iTmFrm[iCurVertexNum][1] << "]" << endl;
        cout << endl;
    }

    
    
    while (iVertexEnergy){
        iVertexCnt++;
        // Force Table Init
        fnFrcTblInit();
        // Operant Distribution Graph
        fnOpDistGrInit();


        if (iDbg) cout << endl << "loop # = " << iVertexCnt << endl << endl;
        // Force Table Calculation
        for (iCurVertexNum = 1; iCurVertexNum <= iVerticeNum; iCurVertexNum++){
            compCurType = mapFDS.find(iCurVertexNum)->second.getType();
            iTmFrmSt = iTmFrm[iCurVertexNum][0];
            iTmFrmEd = iTmFrm[iCurVertexNum][1];
            iTmRng = iTmFrmEd - iTmFrmSt + 1 ;
            iCurCompPred = mapFDS.find(iCurVertexNum)->second.getPreNum();
            iCurCompSuc = mapFDS.find(iCurVertexNum)->second.getSucNum();
            if (iTmRng != 1){
                for (dbCurState = iTmFrmSt; dbCurState <= iTmFrmEd; dbCurState++){
                    // Self Force
                    dbSlfFrc = fnFrcCalc(iCurVertexNum, compCurType, iTmFrmSt, iTmFrmEd, dbCurState);
                    // Predecessor Force
                    dbPreFrc = fnPreFrcCalc(iCurVertexNum, dbCurState, iCurCompPred);
                    // Successor Force
                    dbSucFrc = fnSucFrcCalc(iCurVertexNum, dbCurState, iCurCompSuc);
                    // Total Force
                    dbFrcTbl[iCurVertexNum][dbCurState] = dbSlfFrc+dbPreFrc + dbSucFrc;
                    dbSlfFrcTbl[iCurVertexNum][dbCurState] = dbSlfFrc;
                    dbPreFrcTbl[iCurVertexNum][dbCurState] = dbPreFrc;
                    dbSucFrcTbl[iCurVertexNum][dbCurState] = dbSucFrc;
                }
            }
        }
        
        // Output Force Table
        if (iDbg) {
            cout << "<< self force >>" << endl;
            for (iCurVertexNum = 1; iCurVertexNum <= iVerticeNum; iCurVertexNum++){
                for (dbCurState = 1; dbCurState <= iStateNum; dbCurState++){
                    if (dbSlfFrcTbl[iCurVertexNum][dbCurState] == 999)
                        cout << "., ";
                    else
                        cout << dbSlfFrcTbl[iCurVertexNum][dbCurState] << ", ";
                }
                cout << endl;
            }
            cout << "<< pre force >>" << endl;
            for (iCurVertexNum = 1; iCurVertexNum <= iVerticeNum; iCurVertexNum++){
                for (dbCurState = 1; dbCurState <= iStateNum; dbCurState++){
                    if (dbPreFrcTbl[iCurVertexNum][dbCurState] == 999)
                        cout << "., ";
                    else
                        cout << dbPreFrcTbl[iCurVertexNum][dbCurState] << ", ";
                }
                cout << endl;
            }
            cout << "<< suc force >>" << endl;
            for (iCurVertexNum = 1; iCurVertexNum <= iVerticeNum; iCurVertexNum++){
                for (dbCurState = 1; dbCurState <= iStateNum; dbCurState++){
                    if (dbSucFrcTbl[iCurVertexNum][dbCurState] == 999)
                        cout << "., ";
                    else
                        cout << dbSucFrcTbl[iCurVertexNum][dbCurState] << ", ";
                }
                cout << endl;
            }
            
            cout << "Total force" << endl;
            for (iCurVertexNum = 1; iCurVertexNum <= iVerticeNum; iCurVertexNum++){
                for (dbCurState = 1; dbCurState <= iStateNum; dbCurState++){
                    if (dbFrcTbl[iCurVertexNum][dbCurState] == 999)
                        cout << "., ";
                    else
                        cout << dbFrcTbl[iCurVertexNum][dbCurState] << ", ";
                }
                cout << endl;
                
            }
        }

        // Find Least Force
        // fnDbArrMin(dbFrcTbl){
        double dbArrMin = 999;
        int iMinVertexNum = 1;
        double dbCurStateMin = 1;
        // --------------------------------------
        for (iCurVertexNum = 1; iCurVertexNum <= iVerticeNum; iCurVertexNum++){
            for (dbCurState = 1; dbCurState <= iStateNum; dbCurState++){
                if (dbArrMin > dbFrcTbl[iCurVertexNum][dbCurState]){
                    dbArrMin = dbFrcTbl[iCurVertexNum][dbCurState];
                    iMinVertexNum = iCurVertexNum;
                    dbCurStateMin = dbCurState;
                }
            }
        }

        // Update New Time Frame Table
        // Self Vertex Time Frame
        iTmFrm[iMinVertexNum][0] = dbCurStateMin;
        iTmFrm[iMinVertexNum][1] = dbCurStateMin;
        dbCurCompLat = (double)mapFDS.find(iMinVertexNum)->second.getLat();



        // Predecessor Vertex Time Frame
        int iCurPred, iCurPreTmFrmSt, iCurPreTmFrmEt;
        double dbCurPreLat;
        long lSbSz = iCurCompPred.size();
        long lSbsSz = lSbSz;
        int iLyrCnt;
        double dbCurStateMinLp;
        int iCurSrcSt, iCurSrcEt;
        vector<int> iSrc, iSrcSb, iSbSb;
        iCurCompPred = mapFDS.find(iMinVertexNum)->second.getPreNum();
        iSrc.push_back(iMinVertexNum);
        iLyrCnt = 0;
        dbCurStateMinLp = dbCurStateMin;
        while (lSbsSz) {
            lSbsSz = 0;
            iLyrCnt++;
            for (i = 0; i < iSrc.size(); i++){
                vector<int> iPredNumLp = mapFDS.find(iSrc[i])->second.getPreNum();
                iCurSrcSt= iTmFrm[iSrc[i]][0];
                iCurSrcEt= iTmFrm[iSrc[i]][1];
                dbCurStateMinLp = iCurSrcEt;
                //if(iPredNumLp.size()){
                    for (j = 0; j < iPredNumLp.size(); j++) {
						//cout << "Layer " << iLyrCnt << ",  Current Pred Vertex = " << iPredNumLp[j] << endl;
						if (iPredNumLp[j]){
                            iSbSb = mapFDS.find(iPredNumLp[j])->second.getPreNum();
                            iCurPred = iPredNumLp[j];
                            if (iCurPred){
                                iCurPreTmFrmSt = iTmFrm[iCurPred][0];
                                iCurPreTmFrmEt = iTmFrm[iCurPred][1];
                                dbCurPreLat = (double)mapFDS.find(iCurPred)->second.getLat();
                                if (iCurPreTmFrmSt != iCurPreTmFrmEt){
                                    iTmFrm[iCurPred][1] = dbCurStateMinLp-dbCurPreLat;
                                    //cout << "Pred: " << iCurPred << endl;
                                }
                            }
                            if (iSbSb.size()){
                                if (fnIntVectorMax(iSbSb)){
                                    lSbsSz = lSbsSz + iSbSb.size();
                                }
                            }
                            if (iPredNumLp[j])
                                iSrcSb.push_back(iPredNumLp[j]);
                    }
				}
            }
            iSrc=iSrcSb;
            iSrcSb.clear();
        }

        // Successor Vertex Time Frame
        int iCurSuc, iCurSucTmFrmSt, iCurSucTmFrmEt;;
        double dbCurCompLat, dbCurSucLat;
        iCurCompSuc = mapFDS.find(iMinVertexNum)->second.getSucNum();
        iSrc.clear();
        iSrc.push_back(iMinVertexNum);
        lSbSz = iCurCompSuc.size();
        lSbsSz = lSbSz;
        iLyrCnt=0;
        
        dbCurStateMinLp = dbCurStateMin;
        while (lSbsSz) {
            lSbsSz = 0;
            iLyrCnt++;
            for (i = 0; i < iSrc.size(); i++){
                vector<int> iSucNumLp = mapFDS.find(iSrc[i])->second.getSucNum();
                iCurSrcSt= iTmFrm[iSrc[i]][0];
                iCurSrcEt= iTmFrm[iSrc[i]][1];
                dbCurStateMinLp = iCurSrcSt;
                for (j = 0; j < iSucNumLp.size(); j++) {
                    //cout << "Layer " << iLyrCnt << ",  Current Succ Vertex = " << iSucNumLp[j] << endl;
                    if(iSucNumLp[j]){
                        iSbSb = mapFDS.find(iSucNumLp[j])->second.getSucNum();
                        iCurSuc = iSucNumLp[j];
                        if (iCurSuc){
                            iCurSucTmFrmSt = iTmFrm[iCurSuc][0];
                            iCurSucTmFrmEt = iTmFrm[iCurSuc][1];
                            dbCurCompLat = (double)mapFDS.find(iSrc[i])->second.getLat();
                            dbCurSucLat = (double)mapFDS.find(iCurSuc)->second.getLat();
                            if (iCurSucTmFrmSt != iCurSucTmFrmEt){
                                iTmFrm[iCurSuc][0] = dbCurStateMinLp + dbCurCompLat;
                                //cout << "Suc: " << iCurSuc << endl;
                            }
                        }
                        if (iSbSb.size()){
                            if (fnIntVectorMax(iSbSb)){
                                lSbsSz = lSbsSz + iSbSb.size();
                            }
                        }
                        if (iSucNumLp[j])
                            iSrcSb.push_back(iSucNumLp[j]);
                    }
                }
			}
            iSrc=iSrcSb;
            iSrcSb.clear();
        }

        
        // Vertex Finish Count
        iVertexEnergy = 0;
        for (i = 1; i <= iVerticeNum; i++){
            iVertexEnergy = iVertexEnergy + (iTmFrm[i][1] - iTmFrm[i][0]);
        }
        // Output Current Time Frame
        if (iDbg) {
            cout << "iMinVertexNum = " << iMinVertexNum << endl;
            cout << "<<Time Frame>>" << endl;
            for (iCurVertexNum = 1; iCurVertexNum <= iVerticeNum; iCurVertexNum++)
                cout << "[" << iTmFrm[iCurVertexNum][0] << ", " << iTmFrm[iCurVertexNum][1] << "]" << endl;
            cout << endl;
        }
    }
    // mapFDS update
    for (iCurVertexNum = 1; iCurVertexNum <= iVerticeNum; iCurVertexNum++){
        dbCurLat = mapFDS.find(iCurVertexNum)->second.getLat();
        mapFDS.find(iCurVertexNum)->second.setCDFGState(iTmFrm[iCurVertexNum][0], dbCurLat);
    }
    
    // Output Final Scheduled Result
    if (iDbg) {
        for (iCurVertexNum = 1; iCurVertexNum <= iVerticeNum; iCurVertexNum++){
            dbCurLat = mapFDS.find(iCurVertexNum)->second.getLat();
            iTmFrmSt = iTmFrm[iCurVertexNum][0];
            iTmFrmEd = iTmFrm[iCurVertexNum][1]+dbCurLat-1;
            for (dbCurState = iTmFrmSt; dbCurState <= iTmFrmEd; dbCurState++ )
                dbOutputTbl[dbCurState][iCurVertexNum] = iCurVertexNum;
        }
        cout << "Scheduled Result" << endl;
        for (dbCurState = 1; dbCurState <= dbSysLat; dbCurState++ ){
            for (iCurVertexNum = 1; iCurVertexNum <= iVerticeNum; iCurVertexNum++){
                
                if (dbOutputTbl[dbCurState][iCurVertexNum] < 999){
                    cout << dbOutputTbl[dbCurState][iCurVertexNum] << " ";
                }else{
                    if (iCurVertexNum < 10)
                        cout << ". ";
                    else
                        cout << " . ";
                }
            }
            cout << endl;
        }
    }
 
    
}

//-----------------------------------------------------------//
//  Sub Function Name: fnSucFrcCalc
//  Target: Successor Force Calculation
//  Input:  iCurVertexNum, , iCurCompSuc,
//  Output:
//          dbSucFrc
//-----------------------------------------------------------//
double fnSucFrcCalc(int iCurVertexNum, double dbCurState, vector<int> iCurCompSuc){
    int iCurSuc;
    CompType compSucType;
    double dbCurCompLat;
    double iCurSucTmFrmSt, iCurSucTmFrmEt;
    double dbSucFrc = 0;
    
    if (iCurCompSuc.size()){
        vector<double> dbCurSucFrc;
        for (int iSucIdx = 0; iSucIdx < iCurCompSuc.size(); iSucIdx++){
            iCurSuc = iCurCompSuc[iSucIdx];
            
            if (iCurSuc){
                iCurSucTmFrmSt = iTmFrm[iCurSuc][0];
                iCurSucTmFrmEt = iTmFrm[iCurSuc][1];
                dbCurCompLat = (double)mapFDS.find(iCurVertexNum)->second.getLat();
                compSucType = mapFDS.find(iCurSuc)->second.getType();
                if (iCurSucTmFrmEt == (dbCurState + dbCurCompLat)) {
                    dbSucFrc = dbSucFrc + fnFrcCalc(iCurSuc, compSucType, iCurSucTmFrmSt, iCurSucTmFrmEt, iCurSucTmFrmEt);
                }
            }
        }
    }
    return dbSucFrc;
}

//-----------------------------------------------------------//
//  Sub Function Name: fnPreFrcCalc
//  Target: Preecessor Force Calculation
//  Input:  iCurVertexNum, dbCurState, iCurCompPred
//  Output:
//          dbPreFrc
//-----------------------------------------------------------//
double fnPreFrcCalc(int iCurVertexNum, double dbCurState, vector<int> iCurCompPre){
    int iCurPre;
    CompType compPreType;
    double dbCurPreLat;
    double iCurPreTmFrmSt, iCurPreTmFrmEt;
    double dbPreFrc = 0;
    vector<double> dbCurPreFrc;
    
    if (iCurCompPre.size()){
        for (int iPreIdx = 0; iPreIdx < iCurCompPre.size(); iPreIdx++){
            iCurPre = iCurCompPre[iPreIdx];
            if (iCurPre){
                iCurPreTmFrmSt = iTmFrm[iCurPre][0];
                iCurPreTmFrmEt = iTmFrm[iCurPre][1];
                dbCurPreLat = (double)mapFDS.find(iCurPre)->second.getLat();
                compPreType = mapFDS.find(iCurPre)->second.getType();
                
                if ((iCurPreTmFrmSt+dbCurPreLat) == dbCurState) {
                    dbPreFrc = dbPreFrc + fnFrcCalc(iCurPre, compPreType, iCurPreTmFrmSt, iCurPreTmFrmEt, iCurPreTmFrmSt);
                }
            }
        }
    }
    return dbPreFrc;
}


//-----------------------------------------------------------//
//  Sub Function Name: fnFrcCalc
//  Target: Force Calculation
//  Input:  iCurVertexNum, compCurType, iTmFrmSt, iTmFrmEd
//  Output:
//          dbFrc
//-----------------------------------------------------------//
double fnFrcCalc(int iCurVertexNum, CompType compCurType, int iTmFrmSt, int iTmFrmEd, double dbCurState){
    double dbFrc = 0;
    double dbOpPrb, dbDGvle;
    dbCurLat = (double)mapFDS.find(iCurVertexNum)->second.getLat();
    for (double dbSttLp = iTmFrmSt; dbSttLp < (iTmFrmEd + dbCurLat); dbSttLp++){
        // Get Op Prob and Distrbution Graph Value
        switch (compCurType){
            case 1:         // Add
                dbOpPrb = dbAddDistGr[iCurVertexNum][dbSttLp];
                dbDGvle = dbAddDistGr[0][dbSttLp];
                break;
            case 2:         // Mul
                dbOpPrb = dbMulDistGr[iCurVertexNum][dbSttLp];
                dbDGvle = dbMulDistGr[0][dbSttLp];
                break;
            case 3:         // Div
                dbOpPrb = dbDivDistGr[iCurVertexNum][dbSttLp];
                dbDGvle = dbDivDistGr[0][dbSttLp];
                break;
            default:        // Alu
                dbOpPrb = dbAluDistGr[iCurVertexNum][dbSttLp];
                dbDGvle = dbAluDistGr[0][dbSttLp];
                break;
        }
        
        // Calculate Self Force
        if ((dbSttLp >= dbCurState) && (dbSttLp < dbCurState + dbCurLat))
            dbFrc = dbFrc + dbDGvle*(1/dbCurLat - dbOpPrb);
        else
            dbFrc = dbFrc + dbDGvle*(0 - dbOpPrb);
    }
    return dbFrc;
}

//-----------------------------------------------------------//
//  Sub Function Name: fnFrcTblInit
//  Target: Force Table Init
//  Input: dbFrcTbl
//  Output:
//          dbFrcTbl = 999
//-----------------------------------------------------------//
void fnFrcTblInit(){
    for (iCurVertexNum = 1; iCurVertexNum <= iVerticeNum; iCurVertexNum++){
        for (dbCurState = 0; dbCurState <= iStateNum; dbCurState++){
            dbFrcTbl[iCurVertexNum][dbCurState] = 999;
            dbSlfFrcTbl[iCurVertexNum][dbCurState] = 999;
            dbPreFrcTbl[iCurVertexNum][dbCurState] = 999;
            dbSucFrcTbl[iCurVertexNum][dbCurState] = 999;
            dbOutputTbl[dbCurState][iCurVertexNum] = 999;
        }
    }
}

//-----------------------------------------------------------//
//  Sub Function Name: fnOpDistGrInit
//  Target: Op Probability and Distribution Graph Table Init
//  Input: mapFDS
//  Output:
//          dbMulDistGr, dbAddDistGr, dbDivDistGr, dbAluDistGr
//-----------------------------------------------------------//
void fnOpDistGrInit(){
    int iTmFrmSt, iTmFrmEt;
    double dbVerPrb;
    for (iCurVertexNum = 0; iCurVertexNum <= iVerticeNum; iCurVertexNum++){
        for (dbCurState = 0; dbCurState <= dbSysLat; dbCurState++){
            dbAddDistGr[iCurVertexNum][dbCurState] = 0;
            dbMulDistGr[iCurVertexNum][dbCurState] = 0;
            dbDivDistGr[iCurVertexNum][dbCurState] = 0;
            dbAluDistGr[iCurVertexNum][dbCurState] = 0;
        }
    }

    
    for (iCurVertexNum = 1; iCurVertexNum <= iVerticeNum; iCurVertexNum++){
        compCurType = mapFDS.find(iCurVertexNum)->second.getType();
        dbCurLat = (double)mapFDS.find(iCurVertexNum)->second.getLat();
        //iTmRng = iTmFrm[iCurVertexNum][1] - iTmFrm[iCurVertexNum][0] + 1 ;
        iTmFrmSt = iTmFrm[iCurVertexNum][0];
        iTmFrmEt = iTmFrm[iCurVertexNum][1]+dbCurLat-1;
        iTmRng = iTmFrmEt - iTmFrmSt + 1;

        
        for (int iv = iTmFrmSt; iv <= iTmFrmEt; iv++){
            // option 1
            dbVerPrb = 1/ iTmRng;
            // option2
                /*
                if ((iv < iTmFrmSt+dbCurLat))
                    dbVerPrb = (iv - iTmFrmSt + 1)/ iTmRng / dbCurLat;
                else if ((iv > iTmFrmEt - dbCurLat))
                    dbVerPrb = (iTmFrmEt - iv + 1)/ iTmRng / dbCurLat;
                else
                    dbVerPrb = 1 / iTmRng ;
                 */

            switch (compCurType){
                case 1:         // Mul
                    dbAddDistGr[iCurVertexNum][iv] = dbVerPrb;
                    break;
                case 2:         // Mul
                    dbMulDistGr[iCurVertexNum][iv] = dbVerPrb;
                    break;
                case 3:         // Div
                    dbDivDistGr[iCurVertexNum][iv] = dbVerPrb;
                    break;
                default:        // Alu
                    dbAluDistGr[iCurVertexNum][iv] = dbVerPrb;
                    break;
            }
        }
    }
    for (i=0;i<=iStateNum;i++){
        for (iCurVertexNum = 1; iCurVertexNum <= iVerticeNum; iCurVertexNum++){
            dbAddDistGr[0][i] = dbAddDistGr[0][i] + dbAddDistGr[iCurVertexNum][i];
            dbMulDistGr[0][i] = dbMulDistGr[0][i] + dbMulDistGr[iCurVertexNum][i];
            dbDivDistGr[0][i] = dbDivDistGr[0][i] + dbDivDistGr[iCurVertexNum][i];
            dbAluDistGr[0][i] = dbAluDistGr[0][i] + dbAluDistGr[iCurVertexNum][i];
        }
    }
    if (iDbg) {
        cout << "dbMulDistGr>> " << endl;
        for (iCurVertexNum = 0; iCurVertexNum <= iVerticeNum; iCurVertexNum++){
            for (i=1;i<=iStateNum;i++)
                cout << dbMulDistGr[iCurVertexNum][i] << ", ";
            cout << endl;
        }
        cout << "dbDivDistGr>> "  << endl;
        for (iCurVertexNum = 0; iCurVertexNum <= iVerticeNum; iCurVertexNum++){
            for (i=1;i<=iStateNum;i++)
                cout << dbDivDistGr[iCurVertexNum][i] << ", ";
            cout << endl;
        }
        cout << "dbAddDistGr>> " << endl;
        for (iCurVertexNum = 0; iCurVertexNum <= iVerticeNum; iCurVertexNum++){
            for (i=1;i<=iStateNum;i++)
                cout << dbAddDistGr[iCurVertexNum][i] << ", ";
            cout << endl;
        }
        cout << "dbAluDistGr>> " << endl;
        for (iCurVertexNum = 0; iCurVertexNum <= iVerticeNum; iCurVertexNum++){
            for (i=1;i<=iStateNum;i++)
                cout << dbAluDistGr[iCurVertexNum][i] << ", ";
            cout << endl;
        }
    }
}

//-----------------------------------------------------------//
//  Sub Function Name: fnTmFrmInit
//  Target: Time Frame Table Init
//  Input: mapASAP, mapALAP
//  Output:
//          iTmFrm
//-----------------------------------------------------------//
void fnTmFrmInit(){
    for (iCurVertexNum = 1; iCurVertexNum <= iVerticeNum; iCurVertexNum++){
        iTmFrm[iCurVertexNum][0] = mapASAP.find(iCurVertexNum)->second.getST();
        iTmFrm[iCurVertexNum][1] = mapALAP.find(iCurVertexNum)->second.getST();
        if (iDbg){
            cout << "[" << iTmFrm[iCurVertexNum][0] << ", " << iTmFrm[iCurVertexNum][1] << "]" << endl;
        }
    }
}

//-----------------------------------------------------------//
//  Sub Function Name: fnALAPInit
//  Target: CDFG Source Initialization
//  Input:  mapCDFGSrc
//  Output:
//          mapALAP >> ALAP map
//-----------------------------------------------------------//

void fnALAPInit(){
    if (bErrorFlag) return;
    // Scanning States
    for (int dbCurState=dbSysLat; dbCurState >0; dbCurState--){
        // Scanning Vertices
        for (int iCurVertex=(int)mapALAP.size(); iCurVertex > 0 ; iCurVertex--){
            // Set Information
            //cout << "dbCurState, iCurVertex] = [" << dbCurState << ", " << iCurVertex << "]" << endl;
            iSucNum = mapALAP.find(iCurVertex)->second.getSucNum();
            iSucNumMin = 0;
            //for (int ix = 0; ix < iSucNum.size(); ix++)
            //    cout << "iSucNum = " << iSucNum[ix] << endl;
            if (iSucNum.size()){
                iSucNumMin = fnIntVectorMin(iSucNum);
            }
            dbCurSt = (double)mapALAP.find(iCurVertex)->second.getST();
            dbCurEt = (double)mapALAP.find(iCurVertex)->second.getET();
            dbCurLat = (double)mapALAP.find(iCurVertex)->second.getLat();
            // ALAP Algorithm
            if (dbCurEt != dbCurState) {
                // Check Pre# = 0 Case
                if (!iSucNum.size()){
                    dbCurSt = dbSysLat+ 1 - dbCurLat;
                    mapALAP.find(iCurVertex)->second.setCDFGState(dbCurSt, dbCurLat);
                }else{
                    dbSucSt = (double)mapALAP.find(iSucNumMin)->second.getST();
                    dbCurSt = dbSucSt - dbCurLat;
                    mapALAP.find(iCurVertex)->second.setCDFGState(dbCurSt, dbCurLat);
                }
            }else{
                mapALAP.find(iCurVertex)->second.setCDFGState(dbCurSt, dbCurLat);
            }
            
            // Output Check
            if (iDbg){
               
                cout << "ALAP" << endl;
                for (int iCurVertex=1; iCurVertex <= mapALAP.size(); iCurVertex++){
                    
                    cout << "Vertex " << iCurVertex << endl << "Start State = " << mapALAP.find(iCurVertex)->second.getST() << endl;
                }
            }
        }
    }
    
    // Output Check
    if (iDbg){
        cout << "ALAP" << endl;
        for (int iCurVertex=1; iCurVertex <= mapALAP.size(); iCurVertex++){

                cout << "Vertex " << iCurVertex << endl << "Start State = " << mapALAP.find(iCurVertex)->second.getST() << endl;
        }
    }
}

//-----------------------------------------------------------//
//  Sub Function Name: fnASAPInit
//  Target: CDFG Source Initialization
//  Input:  mapCDFGSrc
//  Output:
//          mapASAP >> ASAP map
//-----------------------------------------------------------//

void fnASAPInit(){
    if (bErrorFlag) return;
    // Scanning States
    for (int dbCurState=1; dbCurState <= mapASAP.size(); dbCurState++){
        // Scanning Vertices
        for (int iCurVertex=1; iCurVertex <= mapASAP.size(); iCurVertex++){
            // Set Information
            iPreNum = mapASAP.find(iCurVertex)->second.getPreNum();
            iPreNumMax = fnIntVectorMax(iPreNum);
            dbCurSt = (double)mapASAP.find(iCurVertex)->second.getST();
            dbCurLat = (double)mapASAP.find(iCurVertex)->second.getLat();
            // ASAP Algorithm
            if (dbCurSt != dbCurState) {
                // Check Pre# = 0 Case
                if (!iPreNumMax){
                    mapASAP.find(iCurVertex)->second.setCDFGState(1, dbCurLat);
                }else{
                    dbPreEt = (double)mapASAP.find(iPreNumMax)->second.getET();
                    mapASAP.find(iCurVertex)->second.setCDFGState(dbPreEt+1, dbCurLat);
                }
            }
        }
    }
    
    if (iDbg) cout << "ASAP" << endl;
    for (int iCurVertex=1; iCurVertex <= mapASAP.size(); iCurVertex++){
        iASAPET.push_back(mapASAP.find(iCurVertex)->second.getET());
        // set Data to ALAP
        int iCurSt = mapASAP.find(iCurVertex)->second.getST();
        int iCurLat = mapASAP.find(iCurVertex)->second.getLat();
        mapALAP.find(iCurVertex)->second.setCDFGState(iCurSt, iCurLat);
        if (iDbg){
            cout << "Vertex " << iCurVertex << endl << "Start State = " << mapASAP.find(iCurVertex)->second.getST() << endl;
        }
    }
    if (fnIntVectorMax(iASAPET)>dbSysLat){
        bErrorFlag=1;
        cout << "Latency Constrain "<< dbSysLat << " is not possible to make!" << endl << "ASAP Latency = " << fnIntVectorMax(iASAPET) << endl;
    }
}

//-----------------------------------------------------------//
//  Sub Function Name: fnCDFGSrcInit
//  Target: CDFG Source Initialization
//  Input:  mapComp
//  Output:
//          mapCDFGSrc >> CDFG Source map
//-----------------------------------------------------------//
void fnCDFGInit(){
    if (bErrorFlag) return;
    // Parsing mapComp to mapCDFGSrc
    for (int i=1; i <= mapComp.size(); i++){
        // Get Vertex Info
        iCurVertexNum = i;
        cmpCurType = mapComp.find(i)->second.getType();
        iCurCompPred = mapComp.find(i)->second.getPreNum();
        iCurCompSuc = mapComp.find(i)->second.getSucNum();
        dbCurCompLat = fnGetCompLat(cmpCurType);
        
        // mapCDFGSrc Initialization
        mapCDFGSrc.insert(pair<int, clsComp>(iCurVertexNum, clsComp()));
        // mapCDFGSrc Data Info insertion
        mapCDFGSrc.find(i)->second.setCDFGInfo(iCurVertexNum, cmpCurType, dbCurCompLat, iCurVertexNum, iCurCompPred, iCurCompSuc);
        
        // mapASAP Initialization
        mapASAP.insert(pair<int, clsComp>(iCurVertexNum, clsComp()));
        // mapCDFGSrc Data Info insertion
        mapASAP.find(i)->second.setCDFGInfo(iCurVertexNum, cmpCurType, dbCurCompLat, iCurVertexNum, iCurCompPred, iCurCompSuc);

        // mapALAP Initialization
        mapALAP.insert(pair<int, clsComp>(iCurVertexNum, clsComp()));
        // mapCDFGSrc Data Info insertion
        mapALAP.find(i)->second.setCDFGInfo(iCurVertexNum, cmpCurType, dbCurCompLat, iCurVertexNum, iCurCompPred, iCurCompSuc);

        // mapFDS Initialization
        mapFDS.insert(pair<int, clsComp>(iCurVertexNum, clsComp()));
        // mapCDFGSrc Data Info insertion
        mapFDS.find(i)->second.setCDFGInfo(iCurVertexNum, cmpCurType, dbCurCompLat, iCurVertexNum, iCurCompPred, iCurCompSuc);
    }
}

//-----------------------------------------------------------//
//  Sub Function Name: fnTblSzInit
//  Target: Table Size Initialization
//  Input:
//  Output:
//
//-----------------------------------------------------------//
void fnTblSzInit(){
    iTmFrm.resize(iVerticeNum+1, vector<int>(2));
    dbAddDistGr.resize(iVerticeNum+1, vector<double>(iStateNum+1));
    dbMulDistGr.resize(iVerticeNum+1, vector<double>(iStateNum+1));
    dbDivDistGr.resize(iVerticeNum+1, vector<double>(iStateNum+1));
    dbAluDistGr.resize(iVerticeNum+1, vector<double>(iStateNum+1));
    dbFrcTbl.resize(iVerticeNum+1, vector<double>(iStateNum+1));
    dbSlfFrcTbl.resize(iVerticeNum+1, vector<double>(iStateNum+1));
    dbPreFrcTbl.resize(iVerticeNum+1, vector<double>(iStateNum+1));
    dbSucFrcTbl.resize(iVerticeNum+1, vector<double>(iStateNum+1));
    dbOutputTbl.resize(iStateNum+1, vector<double>(iVerticeNum+1));
}




