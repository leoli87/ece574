//-----------------------------------------------------------*/
//  Function: ECE574_LAB3 Control Control Datapath Flow Graph file
//  File Name: CDFG.cpp
//  Edition: 201411021455
//  Date: 2014/11/02
//  Content:
//          extern fnCDFG
//-----------------------------------------------------------*/
#ifndef ECE574_LAB3_CDFG
#define ECE574_LAB3_CDFG

#include "defs.h"
extern map<int, clsComp> mapCDFGSrc;
extern map<int, clsComp> mapASAP;
extern map<int, clsComp> mapALAP;
extern map<int, clsComp> mapFDS;

extern void fnCDFG();

#endif /* defined(ECE574_LAB3_CDFG) */
