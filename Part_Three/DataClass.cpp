//-----------------------------------------------------------*/
//  Function: ECE574_LAB3 Data Classification file
//  File Name: DataClass.cpp
//  Edition: 201411101140
//  Date: 2014/11/10
//  Content:
//          Data Classificagtion Flow
//  New:
//      AoLi Modified
//-----------------------------------------------------------*/

#include "DataClass.h"

int fnDataClass();

//-----------------------------------------------------------//
//  Sub Function Name: fnDataClass
//  Target: Parsing Data from stream to Array
//  Input: Input File Data
//  Output:
//          Data Info>>
//                  Line#cnt, Line_1_Element#cnt~Line_n_Element#cnt
//          Data Array>>
//                  Array 1, Line_1.Element1 ~ m
//                      ...
//                  Array n, Line_n.Element1 ~ m
//-----------------------------------------------------------//
int fnDataClass(){
    string strCurType;
    string strCurFormat;
    CompType curType;
    VarFormat curFormat;
    VarFormat iPreNum1Format;
    VarFormat iPreNum2Format;
    VarFormat iPreNum3Format;
    VarFormat iSucNumFormat;
    VarType iPreNum1Type;
    VarType iPreNum2Type;
    VarType iPreNum3Type;
    VarType iSucNumType;
    int iPreNum1, iPreNum2, iPreNum3;//, iSucNum;
    int iSucNum;
    int iOp1, iOp2, iOp3;
    int i, j;
    string strWr;
	int curLnNum;
    vector<string> strVarSet;
    if (bErrorFlag) return 0;
    if (iDbg) cout << endl << "fnDataClass ----------------------------------------------------- "<<endl;
    
    
    for (i=1;i<=iStrLineNum;i++){
        iVarNum = iFileInfo[i];
        // Set Variable Into Variable Map Class by Type, Format, Name
        if ((strFileArr[i][1]=="input") || (strFileArr[i][1]=="output") || (strFileArr[i][1]=="variable")){    // || (strFileArr[i][1]=="register")  || (strFileArr[i][1]=="reg")){
            for (j=3; j<=iVarNum; j++){
                mapVar.insert (pair<string, clsVar>(strFileArr[i][j], clsVar(strFileArr[i][1], strFileArr[i][2], strFileArr[i][j], 0)));
                strVarSet.push_back(strFileArr[i][j]);
                if (iDbg>1){
                    cout << "[" << i << ", " << j << "], " << "strFileArr[i][j]: " << strFileArr[i][j] << endl;
                    cout << mapVar.find(strFileArr[i][j])->second.getType() << " ";
                    cout << mapVar.find(strFileArr[i][j])->second.getFormat() << " ";
                    cout << mapVar.find(strFileArr[i][j])->second.getName()<< endl;
                }
            }
        }
        // Set Operant Into Compenent Map Class by VericNumber, Type, Format
        else{
            iVerticeNum++;
            // Output Variable Data Format Get

            // check variable exist or not
            curFormat = Invalid;
            for (j = 0; j < strVarSet.size(); j++){
                if(strFileArr[i][1] == strVarSet[j]){
                    strCurFormat = mapVar.find(strFileArr[i][1])->second.getFormat();
                    curFormat = fnVarFormatEnum(strCurFormat);
                    break;
                }
            }
            if (curFormat == 10){
                bErrorFlag = 1;
                cout << "Error: Variable '" << strFileArr[i][1] << "' is not defined." << endl;
                exit(0);
            }
            curFormat = Invalid;
            for (j = 0; j < strVarSet.size(); j++){
                if(strFileArr[i][3] == strVarSet[j]){
                    curFormat = Int1;
                    break;
                }
            }
            if (curFormat == 10){
                bErrorFlag = 1;
                cout << "Error: Variable '" << strFileArr[i][3] << "' is not defined." << endl;
                exit(0);
            }
            if (iVarNum == 5){
                curFormat = Invalid;
                for (j = 0; j < strVarSet.size(); j++){
                    if(strFileArr[i][5] == strVarSet[j]){
                        curFormat = Int1;
                        break;
                    }
                }
                if (curFormat == 10){
                    bErrorFlag = 1;
                    cout << "Error: Variable '" << strFileArr[i][5] << "' is not defined." << endl;
                    exit(0);
                }
            }
            if (iVarNum == 5){
                curFormat = Invalid;
                for (j = 0; j < strVarSet.size(); j++){
                    if(strFileArr[i][5] == strVarSet[j]){
                        curFormat = Int1;
                        break;
                    }
                }
                if (curFormat == 10){
                    bErrorFlag = 1;
                    cout << "Error: Variable '" << strFileArr[i][5] << "' is not defined." << endl;
                    exit(0);
                }
            }

            // Get Element Enum #
            iPreNum1 = mapVar.find(strFileArr[i][3])->second.getSrcCompNum();
            iOp1 = fnCompTypeEnum(strFileArr[i][2]);
            iSucNum = mapVar.find(strFileArr[i][1])->second.getSrcCompNum();
            iPreNum1Format = fnVarFormatEnum(mapVar.find(strFileArr[i][3])->second.getFormat());
            iPreNum1Type = fnVarTypeEnum(mapVar.find(strFileArr[i][3])->second.getType());
            iSucNumType = fnVarTypeEnum(mapVar.find(strFileArr[i][1])->second.getType());
            iSucNumFormat = fnVarFormatEnum(mapVar.find(strFileArr[i][1])->second.getFormat());
            // Operant Mapped into Component Map Class
            switch (iVarNum){
                case 3: // Register Case
                    strCurType = strFileArr[i][2];
                    curType = fnCompTypeEnum(strFileArr[i][2]);
					curLnNum = i;
                    // Error Check
                    if ((iPreNum1Format == 10) || (iSucNumFormat == 10) || (iOp1 == 12)){   // Operant Format Check
                        bErrorFlag = 1;
                        cout << "Error: Operant Format is not correct or Variable is not defined." << endl;
                    }
                    if (iPreNum1Type == 1){                     //  Input Variable can't be Output
                        bErrorFlag = 1;
                        cout << "Error: Input Variable Format can not be Output." << endl;
                    }
                    if (iSucNumType == 0){                      //  Outpur Variable can't be Input
                        bErrorFlag = 1;
                        cout << "Error: Output Variable Format can not be Input." << endl;
                    }
                    if (bErrorFlag){
                        cout << "Line " << i << ": " << strFileArr[i][1] << " " << strFileArr[i][2] << " " << strFileArr[i][3]<< endl;
                        return 0;
                    }
                    // Set Pred Vertices' Succ #
                    if (iPreNum1) mapComp.find(iPreNum1)->second.setSucNum(iVerticeNum);
                    // Insert New Element into Component Class
					mapComp.insert(pair<int, clsComp>(iVerticeNum, clsComp(iVerticeNum, curType, curFormat, curLnNum, iPreNum1)));
                    break;
                case 5: // Operant Case
                    strCurType = strFileArr[i][4];
                    curType = fnCompTypeEnum(strFileArr[i][4]);
					curLnNum = i;
                    iPreNum2 = mapVar.find(strFileArr[i][5])->second.getSrcCompNum();
                    iOp2 = fnCompTypeEnum(strFileArr[i][4]);
                    iPreNum2Format = fnVarFormatEnum(mapVar.find(strFileArr[i][5])->second.getFormat());
                    iPreNum2Type = fnVarTypeEnum(mapVar.find(strFileArr[i][5])->second.getType());
                    if ((curType >=3) & (curType <=5))
                        curFormat = iPreNum1Format>iPreNum2Format?iPreNum1Format:iPreNum2Format;
                    // Error Check
                    if ((iPreNum1Format == 10) || (iPreNum2Format == 10) || (iSucNumFormat == 10) || (iOp1 == 12) || (iOp2 == 12)){ // Operant Format Check
                        bErrorFlag = 1;
                        cout << "Error: Operant Format is not correct or Variable is not defined." << endl;
                    }
                    if ((iPreNum1Type == 1) || (iPreNum2Type == 1)){        //  Input Variable can't be Output
                        bErrorFlag = 1;
                        cout << "Error: Input Variable Format can not be Output." << endl;
                    }
                    if (iSucNumType == 0){                                  //  Outpur Variable can't be Input
                        bErrorFlag = 1;
                        cout << "Error: Output Variable Format can not be Input." << endl;
                    }
                    if (bErrorFlag){
                        cout << "Line " << i << ": " << strFileArr[i][1] << " " << strFileArr[i][2] << " " << strFileArr[i][3] << " " << strFileArr[i][4] << " " << strFileArr[i][5]<< endl;
                        return 0;
                    }
                    // Set Pred Vertices' Succ #
                    if (iPreNum1) mapComp.find(iPreNum1)->second.setSucNum(iVerticeNum);
                    if (iPreNum2) mapComp.find(iPreNum2)->second.setSucNum(iVerticeNum);
                    // Insert New Element into Component Class
                    /*// Output wire check
                     if (iSucNumType == 1){
                     strWr = strFileArr[i][1]+ "wire";
                     while (1) {
                     if (mapVar.find(strWr)->second.getName() == strWr)
                     strWr = strWr + "0";
                     else break;
                     }
                     //1. add new wire op in the bottum
                     iFileInfo[0] += 1;
                     iFileInfo.push_back(3);
                     strFileArr[iFileInfo[0]] = new string[4];
                     strFileArr[iFileInfo[0]][1] = strFileArr[i][1];
                     strFileArr[iFileInfo[0]][2] = "=";
                     strFileArr[iFileInfo[0]][3] = strWr;
                     //2. add wire case in array
                     iFileInfo[0] += 1;
                     iFileInfo.push_back(3);
                     strFileArr[iFileInfo[0]][1] = "wire";
                     strFileArr[iFileInfo[0]][2] = mapVar.find(strFileArr[i][1])->second.getFormat();
                     strFileArr[iFileInfo[0]][3] = strWr;
                     mapVar.insert (pair<string, clsVar>(strFileArr[iFileInfo[0]][3], clsVar(strFileArr[iFileInfo[0]][1], strFileArr[iFileInfo[0]][2], strFileArr[iFileInfo[0]][3], iVerticeNum)));
                     //3. rename output to be " "+wire
                     strFileArr[i][1] = strWr;
                     //4. increas Line num
                     iStrLineNum ++;
                     //5. add new comp
                     mapComp.insert(pair<int, clsComp>(iVerticeNum, clsComp(iVerticeNum, curType, curFormat,iPreNum1, iPreNum2)));
                     }
                     else{*/
					mapComp.insert(pair<int, clsComp>(iVerticeNum, clsComp(iVerticeNum, curType, curFormat, curLnNum, iPreNum1, iPreNum2)));
                    //}
                    break;
                case 7: // Compare Case
                    strCurType = strFileArr[i][4];
                    curType = fnCompTypeEnum(strFileArr[i][4]);
					curLnNum = i;
                    iPreNum2 = mapVar.find(strFileArr[i][5])->second.getSrcCompNum();
                    iOp2 = fnCompTypeEnum(strFileArr[i][4]);
                    iPreNum2Format = fnVarFormatEnum(mapVar.find(strFileArr[i][5])->second.getFormat());
                    iPreNum2Type = fnVarTypeEnum(mapVar.find(strFileArr[i][5])->second.getType());
                    iPreNum3 = mapVar.find(strFileArr[i][7])->second.getSrcCompNum();
                    iOp3 = fnCompTypeEnum(strFileArr[i][6]);
                    iPreNum3Format = fnVarFormatEnum(mapVar.find(strFileArr[i][7])->second.getFormat());
                    iPreNum3Type = fnVarTypeEnum(mapVar.find(strFileArr[i][7])->second.getType());
                    if ((iPreNum1Format == 10) || (iPreNum2Format == 10) || (iPreNum3Format == 10) || (iSucNumFormat == 10) || (iOp1 == 12) || (iOp2 == 12) || (iOp3 == 12)){   // Operant Format Check
                        bErrorFlag = 1;
                        cout << "Error: Operant Format is not correct." << endl;
                        return 0;
                    }
                    //  Input Variable can't be Output
                    if ((iPreNum1Type == 1) || (iPreNum2Type == 1)  || (iPreNum3Type == 1)){
                        bErrorFlag = 1;
                        cout << "Error: Input Variable Format can not be Output." << endl;
                    }
                    if (iSucNumType == 0){                                  //  Outpur Variable can't be Input
                        bErrorFlag = 1;
                        cout << "Error: Output Variable Format can not be Input." << endl;
                    }
                    if (bErrorFlag){
                        cout << "Line " << i << ": " << strFileArr[i][1] << " " << strFileArr[i][2] << " " << strFileArr[i][3] << " " << strFileArr[i][4] << " " << strFileArr[i][5] << " " << strFileArr[i][6] << " " << strFileArr[i][7]<< endl;
                        return 0;
                    }
                    
                    // Set Pred Vertices' Succ #
                    if (iPreNum1) mapComp.find(iPreNum1)->second.setSucNum(iVerticeNum);
                    if (iPreNum2) mapComp.find(iPreNum2)->second.setSucNum(iVerticeNum);
                    if (iPreNum3) mapComp.find(iPreNum3)->second.setSucNum(iVerticeNum);
                    // Insert New Element into Component Class
                    
                    /*   // Output wire check
                     if (iSucNumType == 1){
                     strWr = strFileArr[i][1]+ "wire";
                     //1. add new case in the bottum
                     iFileInfo[0] += 1;
                     iFileInfo.push_back(3);
                     strFileArr[iFileInfo[0]] = new string[4];
                     strFileArr[iFileInfo[0]][1] = strFileArr[i][1];
                     strFileArr[iFileInfo[0]][2] = "=";
                     strFileArr[iFileInfo[0]][3] = strFileArr[i][1] + "wire";
                     //2. add wire case in array
                     iFileInfo[0] += 1;
                     iFileInfo.push_back(3);
                     strFileArr[iFileInfo[0]] = new string[4];
                     strFileArr[iFileInfo[0]][1] = "wire";
                     strFileArr[iFileInfo[0]][2] = mapVar.find(strFileArr[i][1])->second.getFormat();
                     strFileArr[iFileInfo[0]][3] = strFileArr[i][1] + "wire";
                     mapVar.insert (pair<string, clsVar>(strFileArr[iFileInfo[0]][3], clsVar(strFileArr[iFileInfo[0]][1], strFileArr[iFileInfo[0]][2], strFileArr[iFileInfo[0]][3], iVerticeNum)));
                     //3. rename output to be " "+wire
                     strFileArr[i][1] = strFileArr[i][1] + "wire";
                     //4. increas Line num
                     iStrLineNum ++;
                     //5. add new comp
                     mapComp.insert(pair<int, clsComp>(iVerticeNum, clsComp(iVerticeNum, curType, curFormat,iPreNum1, iPreNum2, iPreNum3)));
                     
                     }else{ */
					mapComp.insert(pair<int, clsComp>(iVerticeNum, clsComp(iVerticeNum, curType, curFormat, curLnNum, iPreNum1, iPreNum2, iPreNum3)));
                    // }
                    break;
                default:
                    bErrorFlag = 1;
                    cout << "Error: Operant Format is not corrext." << endl;
                    return 0;
                    break;
            }
            mapVar.find(strFileArr[i][1])->second.setSrcCompNum(iVerticeNum);
        }
    }
    for (i=1;i<=iVerticeNum;i++){
        // Debug--------------------------------------------------------- //
        if (iDbg>1){
            //cout << "[" << i << ", 1], " << "strFileArr[i][1]: " << strFileArr[i][1] << endl;
            cout << "Vertex Num : " << mapComp.find(i)->second.getVerticeNum() << " " << endl;
            cout << "strCurType : " << mapComp.find(i)->second.getType() << " " << endl;
            cout << "strCurFormat : " << mapComp.find(i)->second.getFormat()<< endl;
            cout << "Delay : "<< mapComp.find(i)->second.getDelay()<< endl;
            vector<int> iPreNum = mapComp.find(i)->second.getPreNum();
            for (int j=0; j<iPreNum.size(); j++)
                cout << "Pred Vertices Num = " << iPreNum[j] << endl;
            vector<int> iSucNum = mapComp.find(i)->second.getSucNum();
            for (int j=0; j<iSucNum.size(); j++)
                cout << "Succ Vertices Num = " << iSucNum[j] << endl;
            cout << endl;
        }
    }
    
    // Debug--------------------------------------------------------- //
    if (iDbg){
        cout << "<< File to Array Output >>" << endl;
        int iLineNum = 0;
        while (iLineNum<iFileInfo[0])
        {
            iLineNum++;
            if (iDbg>1)
                cout << ">>iLineNum = " << iLineNum << " >>iLnElmtNum = " << iFileInfo[iLineNum] << endl;
            int iLnElmtNum = 0;
            while (iLnElmtNum<iFileInfo[iLineNum])
            {
                iLnElmtNum++;
                
                cout << strFileArr[iLineNum][iLnElmtNum] << " ";
                
            }
            cout << endl;
        }
    }
    
    return 0;
}