//-----------------------------------------------------------*/
//  Function: ECE574_LAB4 Variables Definition Header file
//  File Name: defs.h
//  Edition: 201412092140
//  Date: 2014/12/09
//  Content:
//          Variables Definition Extern
//  New:
//          Add compCls fnSetPreNum
//-----------------------------------------------------------*/

#ifndef ECE574_LAB3_defs
#define ECE574_LAB3_defs
#include "libs.h"

//- System
extern int iDbg;               // Debug Level 0: off(Default), 1: Level1, 2: Level2
extern bool bBltInSrc;         // Bulit In Source Flag 0: Outside Input(Default), 1:Built In Source
extern bool bErrorFlag;        // Error Flag, Initially no Error

//- Input/Output File Name
extern fstream cFile;   //define two objections.  cFile is the source;verilogFile object.
extern fstream verilogFile;

//- Component Type
enum VarType{ input, output, reg, NotSupport};
enum VarFormat{ Int1, Int8, Int16, Int32, Int64, UInt1, UInt8, UInt16, UInt32, UInt64, Invalid};
enum CompType{ REG, ADD, SUB, COMPGT, COMPLT, COMPEQ, MUX, SHR, SHL, MUL, DIV, MOD, IF, ELSE, WHILE, END, NotDefine};
//- Component Array
extern string strFormat[11];
extern string strType[17];
extern string InandOut[36];
//- Delay Table [CompType][VarFormat]
extern double dbDelayTable[16][10];

//- Global Index
extern int iCompNum;

//- Data Parser Info
extern vector<int> iFileInfo;
extern string ** strFileArr;
extern int iStrLineNum, iVarNum;
extern int iVerticeNum;
extern fstream strFile;
extern int c;
extern int z;
extern int i, j;
extern string strName;
extern double dbSysLat;


// Class ----------------------------------------------------//
//-----------------------------------------------------------//
//  Class Definition: clsVar
//  Target: Set Variable Class
//  Member:
//          Name: strName
//          Format: strFormat
//          Type: strType
//-----------------------------------------------------------//
class clsVar
{
public:
    string strName;         // ex: a
    string strFormat;       // ex: Int8
    string strType;     // ex: Input
    int iSrcCompNum;
    
    string getName(){
        return strName;
    }
    
    string getFormat(){
        if(!strFormat.size())
            strFormat = "#Invalid";
        return strFormat;
    }
    
    string getType(){
        if (!strType.size())
            strType = "NotDefine";
        return strType;
    }
    
    int getSrcCompNum(){
        return iSrcCompNum;
    }
    
    int setSrcCompNum(int iSrcCompNum){
        this->iSrcCompNum = iSrcCompNum;
        return iSrcCompNum;
    }
    
    clsVar() {}
    
    clsVar(string strType, string strFormat, string strName, int iSrcCompNum){
        this->strName = strName;
        this->strFormat = strFormat;
        this->strType = strType;
        this->iSrcCompNum = iSrcCompNum;
    }
};

//-----------------------------------------------------------//
//  Class Definition: clsComp
//  Target: Set Component Class
//  Member:
//          Label: iVerticeNum
//          Format: strFormat
//          Type: strType
//          Delay: dbDelay
//          Input Number: iInputNum
//          Input: strInput
//          Output: strOutput
//          Predecessor: strPred
//          Successor: strSucc
//-----------------------------------------------------------//
class clsComp
{
public:
    int iVerticeNum;
    CompType strType;
    VarFormat strFormat;
    double dbDelay;
    vector<int> iPreNum;    //  Predecessor Vertex Number
    vector<int> iSucNum;    //  Successor Vertex Number
    vector<int>::iterator iPreNumSt;
    
    int iPreSize;           //  Predecessor Vertex Size
    int iST;        // Start Time
    int iET;        // End Time
    int iLat;       // Op Latency
    int iLnNum;    // the Line Number in the strFileArr[][]
        
    int getVerticeNum(){
        return iVerticeNum;
    }
    
    VarFormat getFormat(){
        return strFormat;
    }
    
    CompType getType(){
        return strType;
    }
    
    double getDelay(){
        return dbDelay;
    }
    
    vector<int> getPreNum(){
        return iPreNum;
    }
    
    vector<int>  getSucNum(){
        return iSucNum;
    }
    
    int getST(){
        return iST;
    }
    
    int getET(){
        return iET;
    }
    
    int getLat(){
        return iLat;
    }
    
    int getLnNum(){                    //
	return iLnNum;
    }
    
    vector<int> setSucNum(int iSucNum1){
        iSucNum.push_back (iSucNum1);
        return iSucNum;
    }
    
    vector<int> setPreNum(int iPreNum1){
        iPreNumSt = iPreNum.begin();
        iPreNum.insert(iPreNumSt, iPreNum1);//(iPos, iPreNum1);
        return iPreNum;
    }
    
    clsComp() {}
    
    // 1 Input
	clsComp(int iVerticeNum, CompType strType, VarFormat strFormat, int iLnNum,  int iPreNum1){  //
        iPreSize = 1;
        iPreNum.push_back (iPreNum1);
		fnCompInfo(iVerticeNum, strType, strFormat, iLnNum);
    }
    // 2 Input
	clsComp(int iVerticeNum, CompType strType, VarFormat strFormat, int iLnNum, int iPreNum1, int iPreNum2){   //
        iPreSize = 2;
        iPreNum.push_back (iPreNum1);
        iPreNum.push_back (iPreNum2);
		fnCompInfo(iVerticeNum, strType, strFormat, iLnNum);
    }
    // 3 Input
	clsComp(int iVerticeNum, CompType strType, VarFormat strFormat, int iLnNum, int iPreNum1, int iPreNum2, int iPreNum3){  // 
        iPreSize = 3;
        iPreNum.push_back (iPreNum1);
        iPreNum.push_back (iPreNum2);
        iPreNum.push_back (iPreNum3);
		fnCompInfo(iVerticeNum, strType, strFormat, iLnNum);
    }
   
    // Set Component Info
	int fnCompInfo(int iVerticeNum, CompType strType, VarFormat strFormat, int iLnNum){
        this->strType = strType;
        this->strFormat = strFormat;
        this->iVerticeNum = iVerticeNum;
		this->iLnNum = iLnNum;   //
        this->dbDelay = dbDelayTable[strType][strFormat];
        // Update Output Variable Source Vertex to be Cuurent Vertex
        if (iDbg>0){
            cout << "Vertex Num = " << iVerticeNum << endl;
            cout << "CompType = " << strType << " VarFormat = " << strFormat << endl;
			cout << "CompLineNumber=" << iLnNum << endl;   //
            cout << "compdelay = " << dbDelay << endl;
        }
        return 0;
    }
    
    // Set CDFG Info
    void setCDFGInfo(int iVerticeNum1, CompType strType1, int iLat1, int iST1, vector<int> iPreNum1, vector<int> iSucNum1){
        int compCurType;
        switch (strType1) {
            case 1:         // MUL
                compCurType = 1;
                break;
            case 2:         // MUL
                compCurType = 1;
                break;
            case 9:         // MUL
                compCurType = 2;
                break;
            case 10:        // DIV
                compCurType = 3;
                break;
            case 11:        // MOD
                compCurType = 3;
                break;
            default:        // Other ALU
                compCurType = 0;
                break;
        }
        this->strType = (CompType)compCurType;
        this->iVerticeNum = iVerticeNum1;
        this->iLat = iLat1;
        this->iST = iST1;
        this->iET = iST1 + iLat1 - 1;
        long iPreSz = iPreNum1.size();
        while (iPreSz) {
            iPreSz = iPreSz- 1;
            iPreNum.push_back (iPreNum1[iPreSz]);
        }
        long iSucSz = iSucNum1.size();
        while (iSucSz) {
            iSucSz = iSucSz- 1;
            iSucNum.push_back (iSucNum1[iSucSz]);
        }
    }
    
    // Set CDFG State
    void setCDFGState(int iST1, int iLat1){
        this->iST = iST1;
        this->iLat = iLat1;
        this->iET = iST1 + iLat1 - 1;
    }
    
};

// map ------------------------------------------------------//
extern map<string, clsVar> mapVar;
extern map<int, clsComp> mapComp;
extern map<int, clsComp>::iterator it;

// function extern ------------------------------------------//
extern VarType fnVarTypeEnum(string strType);
extern CompType fnCompTypeEnum(string strType);
extern VarFormat fnVarFormatEnum(string strFormat);
extern int fnGetCompLat(CompType curCompType);
extern double fnDbVectorMax(vector<double> dbSrc);
extern double fnDbVectorMin(vector<double> dbSrc);
extern int fnIntVectorMax(vector<int> iSrc);
extern int fnIntVectorMin(vector<int> iSrc);
extern double fnDbVectorSum(vector<double> iSrc);
#endif /* defined(__test__par_def__) */
