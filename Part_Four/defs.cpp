//-----------------------------------------------------------*/
//  Function: ECE574_LAB4 Variables Definition file
//  File Name: defs.cpp
//  Edition: 201412021045
//  Date: 2014/12/02
//  Content:
//          Variables Definition
//  New:
//          Add Condition Case
//
//-----------------------------------------------------------*/

#include "defs.h"

//- System
int iDbg = 2;               // Debug Level 0: off(Default), 1: Level1, 2: Level2
bool bErrorFlag = 0;        // Error Flag, Initially no Error

//- Input/Output File Name
fstream cFile;        // netlistFile: source file
fstream verilogFile;        // verilogFile: output file

//- Component Array
string strFormat[11] = { "#(1)", "#(8)", "#(16)", "#(32)", "#(64)", "#(1)", "#(8)", "#(16)", "#(32)", "#(64)", "#Invalid" };
string strType[17] = { "REG", "ADD", "SUB", "COMPGT", "COMPLT", "COMPEQ", "MUX", "SHR", "SHL", "MUL", "DIV", "MOD", "IF", "ELSE", "WHILE", "END",  "NotDefine"};
string InandOut[36] = { "q", "d", "sum", "a", "b", "diff", "a", "b", "gt", "a", "b", "lt", "a", "b", "eq", "a", "b", "d", "sel", "a", "b", "d", "sh_amt", "a", "d", "sh_amt", "a", "prod", "a", "b", "quot", "a", "b", "rem", "a", "b" };
//- Delay Table [CompType][VarFormat]
double dbDelayTable[16][10] = {
    {0.754, 0.776,  0.8,    0.825,  0.828,   0.754, 0.776,  0.8,    0.825,  0.828},
    {0.718,	1.178,	1.293,	1.523,	1.983,   0.718,	1.178,	1.293,	1.523,	1.983},
    {0.718,	1.584,	1.749,	1.939,	2.46,    0.718,	1.584,	1.749,	1.939,	2.46},
    {0.712,	1.997,	1.572,	1.684,	1.908,   0.712,	1.997,	1.572,	1.684,	1.908},
    {0.712,	1.901,	1.529,	1.641,	1.865,   0.712,	1.901,	1.529,	1.641,	1.865},
    {0.712,	1.024,	1.336,	1.408,	1.562,   0.712,	1.024,	1.336,	1.408,	1.562},
    {0.768,	0.792,	0.819,	0.848,	0.852,   0.768,	0.792,	0.819,	0.848,	0.852},
    {0.712,	1.211,	1.44,	1.701,	1.705,   0.712,	1.211,	1.44,	1.701,	1.705},
    {0.712,	1.223,	1.374,	1.696,	1.674,   0.712,	1.223,	1.374,	1.696,	1.674},
    {0.689,	3.131,	3.362,	5.717,	8.169,   0.689,	3.131,	3.362,	5.717,	8.169},
    {0.693,	9.713,	22.916,	52.048,	115.463, 0.693,	9.713,	22.916,	52.048,	115.463},
    {0.928,	10.565,	23.337,	52.461,	115.939, 0.928,	10.565,	23.337,	52.461,	115.939},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0}};

//- Global Index
int iCompNum = 0;
double dbSysLat=0;
//- Data Parser Info
vector<int> iFileInfo;
string ** strFileArr;
int iStrLineNum, iVarNum, iLnNum;
int iVerticeNum = 0;
fstream strFile;
int c=0;
int z = 0;
int i, j;
string strName;

map<string, clsVar> mapVar;
map<int, clsComp> mapComp;
map<int, clsComp>::iterator it;


//-----------------------------------------------------------//
//  Sub Function Name: fnVarTypeEnum
//  Target: Convert Variable Type from String to Enum
//  Input: Input Variable Tyep in String format
//  Output: Input Variable Tyep in Enum format
//-----------------------------------------------------------//
VarType fnVarTypeEnum(string strType){
    VarType varType_e;
    if (strType == "input")
        varType_e = input;      // 0
    else if (strType == "output")
        varType_e = output;     // 1
    else if (strType == "variable")
        varType_e = reg;       // 2
    else
        varType_e = NotSupport; // 3
    return varType_e;
}

//-----------------------------------------------------------//
//  Sub Function Name: fnCompTypeEnum
//  Target: Convert Component Type from String to Enum
//  Input: Input Component Type in String format
//  Output: Input Component Type in Enum format
//-----------------------------------------------------------//
CompType fnCompTypeEnum(string strType){
    CompType compType_e;
    if (strType == "+")
        compType_e = ADD;        // 1
    else if (strType == "-")
        compType_e = SUB;        // 2
    else if (strType == "*")
        compType_e = MUL;        // 9
    else if (strType == "/")
        compType_e = DIV;        // 10
    else if (strType == "%")
        compType_e = MOD;        // 11
    else if (strType == ">")
        compType_e = COMPGT;     // 3
    else if (strType == "==")
        compType_e = COMPEQ;     // 5
    else if (strType == "<")
        compType_e = COMPLT;     // 4
    else if (strType == ">>")
        compType_e = SHR;        // 7
    else if (strType == "<<")
        compType_e = SHL;        // 8
    else if ((strType == "?") || ((strType == ":")))
        compType_e = MUX;        // 6
    else if (strType == "=")
        compType_e = REG;        // 0
    else if (strType == "if")
        compType_e = IF;         // 12
    else if (strType == "else")
        compType_e = ELSE;        // 13
    else if (strType == "while")
        compType_e = WHILE;       // 14
    else if (strType == "}")
        compType_e = END;         // 15
    else
        compType_e = NotDefine;   // 16
    return compType_e;
}

//-----------------------------------------------------------//
//  Sub Function Name: fnGetCompLat
//  Target: Get Component Latency
//  Input: Input Component Type in CompType
//  Output: Input Component Latency
//-----------------------------------------------------------//
int fnGetCompLat(CompType curCompType){
    int iCompLat;
    switch (curCompType) {
        case 1:         // ADD
            iCompLat = 1;
            break;
        case 2:         // SUB
            iCompLat = 1;
            break;
        case 9:         // MUL
            iCompLat = 2;
            break;
        case 10:        // DIV
            iCompLat = 3;
            break;
        case 11:        // MOD
            iCompLat = 3;
            break;
        default:        // Other ALU
            iCompLat = 1;
            break;
    }
    return iCompLat;
}

//-----------------------------------------------------------//
//  Sub Function Name: fnVarFormatEnum
//  Target: Convert Variable Type from String to Enum
//  Input: Input Variable Tyep in String format
//  Output: Input Variable Tyep in Enum format
//-----------------------------------------------------------//
VarFormat fnVarFormatEnum(string strFormat){
    VarFormat verFormat_e;
    if ((strFormat == "Int1") || (strFormat == "bool"))
        verFormat_e = Int1;     // 0
    else if ((strFormat == "UInt1") || (strFormat == "unsigned bool"))
        verFormat_e = UInt1;    // 5
    else if ((strFormat == "Int8") || (strFormat == "char"))
        verFormat_e = Int8;     // 1
    else if ((strFormat == "UInt8") || (strFormat == "unsigned char"))
        verFormat_e = UInt8;    // 6
    else if ((strFormat == "Int16") || (strFormat == "short"))
        verFormat_e = Int16;    // 2
    else if ((strFormat == "UInt16") || (strFormat == "unsigned short"))
        verFormat_e = UInt32;   // 7
    else if ((strFormat == "Int32") || (strFormat == "int"))
        verFormat_e = Int32;    // 3
    else if ((strFormat == "UInt32") || (strFormat == "unsigned int"))
        verFormat_e = UInt64;   // 8
    else if ((strFormat == "Int64") || (strFormat == "long"))
        verFormat_e = Int64;    // 4
    else if ((strFormat == "UInt64") || (strFormat == "unsigned long"))
        verFormat_e = UInt64;   // 9
    else
        verFormat_e = Invalid;  // 10
    return verFormat_e;
}

//-----------------------------------------------------------//
//  Sub Function Name: fnDbVectorMax
//  Target: Find Max Value in an Double Vector
//  Input: Input Vector in Double
//  Output:
//          Max Value of the Input Double Vector
//-----------------------------------------------------------//
double fnDbVectorMax(vector<double> dbSrc){
    double dbVectorMax = dbSrc[1];
    for (int i =0; i < dbSrc.size(); i++)
        dbVectorMax = dbVectorMax > dbSrc[i] ? dbVectorMax : dbSrc[i];
    return dbVectorMax;
}

//-----------------------------------------------------------//
//  Sub Function Name: fnDbVectorMin
//  Target: Find Min Value in an Double Vector
//  Input: Input Vector in Double
//  Output:
//          Min Value of the Input Double Vector
//-----------------------------------------------------------//
double fnDbVectorMin(vector<double> dbSrc){
    double dbVectorMin = dbSrc[0];
    for (int i =0; i < dbSrc.size(); i++)
        dbVectorMin = dbVectorMin < dbSrc[i] ? dbVectorMin : dbSrc[i];
    return dbVectorMin;
}

//-----------------------------------------------------------//
//  Sub Function Name: fnIntVectorMax
//  Target: Find Max Value in an Integer Vector
//  Input: Input Vector in Integer
//  Output:
//          Max Value of the Input Integer Vector
//-----------------------------------------------------------//
int fnIntVectorMax(vector<int> iSrc){
    int iVectorMax = iSrc[0];
    for (int i =0; i < iSrc.size(); i++)
        iVectorMax = iVectorMax > iSrc[i] ? iVectorMax : iSrc[i];
    return iVectorMax;
}

//-----------------------------------------------------------//
//  Sub Function Name: fnIntVectorMin
//  Target: Find Min Value in an Integer Vector
//  Input: Input Vector in Integer
//  Output:
//          Min Value of the Input Integer Vector
//-----------------------------------------------------------//
int fnIntVectorMin(vector<int> iSrc){
    int iVectorMin = iSrc[0];
    for (int i =0; i < iSrc.size(); i++)
        iVectorMin = iVectorMin < iSrc[i] ? iVectorMin : iSrc[i];
    return iVectorMin;
}

//-----------------------------------------------------------//
//  Sub Function Name: fnVectorSum
//  Target: Find Sum Value in an Double Vector
//  Input: Input Vector in Double
//  Output:
//          Sum Value of the Double Integer Vector
//-----------------------------------------------------------//
double fnDbVectorSum(vector<double> iSrc){
    double iVectorSum = iSrc[0];
    for (int i =0; i < iSrc.size(); i++)
        iVectorSum = iVectorSum + iSrc[i];
    return iVectorSum;
}
