//-----------------------------------------------------------*/
//  Function: ECE574_LAB4 Data Classification file
//  File Name: DataClass.cpp
//  Edition: 201411231040
//  Date: 2014/11/23
//  Content:
//          Data Classificagtion Flow
//-----------------------------------------------------------*/

#ifndef ECE574_LAB3_DataClass
#define ECE574_LAB3_DataClass

#include "defs.h"

extern int fnDataClass();

#endif /* defined(__test__DataClass__) */
