//-----------------------------------------------------------*/
//  Function: ECE574_LAB4 Data Output file
//  File Name: DataOutput..cpp
//  Edition: 201411231040
//  Date: 2014/11/23
//  Content:
//          Data Output Flow
//          a.) fnDataWrite
//          b.) detect
//          c.) CopOut
//-----------------------------------------------------------*/

#include "DataOutput.h"
#include "CDFG.h"
void detect();
void fnDataWrite();
int iCurLatout, iCurStout, iCurEtout, iPreEtout, iSucStout, iLineNumberout, iNxtStout, iNxtEtout, iNxVertexNum, iNxStVertexNum;
int iCurMax, iVectorMax, digital, TtState;
int iNxtVertex;
int iLstStVertexNum,iLstStout, iLstLineNum;
int iCurLineNumberout;
string strPredtype;
//void CopOut();
//-----------------------------------------------------------//
//  Sub Function Name: fnDataWrite()
//  Target: Write Data to the verilogFile.txt
//  Input: Input form mapVar and mapComp
//  Output:
//          Data Info>>
//          Data Array>>
//-----------------------------------------------------------//
void fnDataWrite() {
    int a, b;
    //int i, j, a,b;
    a = 0;
    b = 0;
	iVectorMax = 1;
	digital = 0;
	TtState = 0;
	iLineNumberout = 0;
	iNxtVertex = 0;
	iCurStout = 0;
	iCurEtout = 0;
	iNxtStout = 0;
	iNxtEtout = 0;
	iCurLineNumberout = 0;
	iNxVertexNum = 0;
	iNxStVertexNum = 0;
	//find maxmimum state
	for (int iCurVertex = 1; iCurVertex <= mapCDFGSrc.size(); iCurVertex++){
		iCurMax = mapCDFGSrc.find(iCurVertex)->second.getET();
		if (iCurMax > iVectorMax){
			iVectorMax = iCurMax;
		}
	}
    //write module();
    verilogFile << "`timescale 1ns / 1ps" << endl;
    verilogFile << "module" <<" HLSM " << "(Clk, Rst, Start, Done," ;
    for (i = 1; i <= iFileInfo[0]; i++){
        if ((strFileArr[i][1] == "input") || (strFileArr[i][1] == "output")){
            for (j = 1; j <= iFileInfo[i]; j++){
                if ((strFileArr[i][j] != "input") && (strFileArr[i][j] != "output")&&( strFileArr[i][j] != "variable")){
                    if ((strFileArr[i][j] != "bool" )&& (strFileArr[i][j] != "unsigned bool")&&(strFileArr[i][j] != "char") && (strFileArr[i][j] != "unsigned char") && (strFileArr[i][j] != "short")&&(strFileArr[i][j] != "unsigned short") && (strFileArr[i][j] != "int")&&(strFileArr[i][j] != "unsigned int" )&& (strFileArr[i][j] != "long") && (strFileArr[i][j] != "unsigned long")){
                        a = a + 1;
                    }
                }
            }
        }
    }
    for (i = 1; i <= iFileInfo[0]; i++){
        if ((strFileArr[i][1] == "input") || (strFileArr[i][1] == "output")){
            for (j = 1; j <= iFileInfo[i]; j++){
                if ((strFileArr[i][j] != "input") && (strFileArr[i][j] != "output")&& (strFileArr[i][j] != "variable")){
					if ((strFileArr[i][j] != "bool") && (strFileArr[i][j] != "unsigned bool") && (strFileArr[i][j] != "char") && (strFileArr[i][j] != "unsigned char") && (strFileArr[i][j] != "short") && (strFileArr[i][j] != "unsigned short") && (strFileArr[i][j] != "int") && (strFileArr[i][j] != "unsigned int") && (strFileArr[i][j] != "long") && (strFileArr[i][j] != "unsigned long")){
                        
                        verilogFile <<" "<< strFileArr[i][j];
                        
                        if (b < (a-1)){
                            verilogFile << ",";
                            b ++;
                        }
                    }
                }
            }
        }
    }
    verilogFile << ")" << ";"<<endl;
    
    //write input,output,reg
    verilogFile << "input Clk, Rst, Start;" << endl;
    verilogFile << "output reg Done;" << endl;
    for (i = 1; i <= iFileInfo[0]; i++){
        if (strFileArr[i][1] == "input" || strFileArr[i][1] == "output" || strFileArr[i][1] == "variable"){
            for (j = 1; j <= iFileInfo[i]; j++){
                if (j == 1){
                    if (strFileArr[i][1] == "variable")
                    {
                        verilogFile << "reg ";
                    }
					else if (strFileArr[i][1] == "output")
					{
						verilogFile << "output reg ";
					}
					else 
                    {
                        verilogFile << strFileArr[i][j] << " ";
                    }
                }
                else if (j == 2)
                {
                    if (strFileArr[i][j] == "bool")
                    {
                        verilogFile << "signed" << " " << "[" << "0" << ":" << "0" << "]" << " ";
                    }
                    else if (strFileArr[i][j] == "char")
                    {
                        verilogFile << "signed" << " " << "[" << "7" << ":" << "0" << "]" << " ";
                    }
                    else if (strFileArr[i][j] == "short")
                    {
                        verilogFile << "signed" << " " << "[" << "15" << ":" << "0" << "]" << " ";
                    }
                    else if (strFileArr[i][j] == "int")
                    {
                        verilogFile << "signed" << " " << "[" << "31" << ":" << "0" << "]" << " ";
                    }
                    else if (strFileArr[i][j] == "long")
                    {
                        verilogFile << "signed" << " " << "[" << "63" << ":" << "0" << "]" << " ";
                    }
                    else if (strFileArr[i][j] == "unsigned bool")
                    {
                        verilogFile << "unsigned" << " " << "[" << "0" << ":" << "0" << "]" << " ";
                    }
                    else if (strFileArr[i][j] == "unsigned char")
                    {
                        verilogFile << "unsigned" << " " << "[" << "7" << ":" << "0" << "]" << " ";
                    }
                    else if (strFileArr[i][j] == "unsigned short")
                    {
                        verilogFile << "unsigned" << " " << "[" << "15" << ":" << "0" << "]" << " ";
                    }
                    else if (strFileArr[i][j] == "unsigned int")
                    {
                        verilogFile << "unsigned" << " " << "[" << "31" << ":" << "0" << "]" << " ";
                    }
                    else if (strFileArr[i][j] == "unsigned long")
                    {
                        verilogFile << "unsigned" << " " << "[" << "63" << ":" << "0" << "]" << " ";
                    }
                }
                else
                {
                    verilogFile << strFileArr[i][j];
                    
                    if (j < iFileInfo[i]){
                        verilogFile << ", ";
                        
                    }
                }
            }
            verilogFile << ";" << endl;
        }
    }

	//declared State
	TtState = iVectorMax + 2;
	if (TtState <= 2){
		digital = 0;
	}
	else
	{
		while (TtState > 2){
			TtState = TtState / 2;
			digital++;
		}
	}
	verilogFile << "reg " << "[" << digital << ":0]" << " State;" << endl;
	//

    // write parameter S0 S1..
	verilogFile << "parameter ";
	verilogFile << "Wait" << "=0,";
	for (int State = 1; State <= iVectorMax; State++){
		verilogFile << "S" << State-1 <<"="<<State <<", ";
	}
	verilogFile << "Final" << "=" << iVectorMax + 1 << ";" << endl;

	verilogFile << "always @(posedge Clk) begin" << endl;

	//Rst state
	verilogFile << "if(Rst==1) begin" << endl;
	for (i = 1; i <= iFileInfo[0]; i++){
		if (strFileArr[i][1] == "output" || strFileArr[i][1] == "variable"){
			for (j = 3; j <= iFileInfo[i]; j++)
			{
				verilogFile <<"                "<< strFileArr[i][j] << "<=0;" << endl;
			}
		}
	}
	verilogFile << "                " << "State<=Wait;" << endl;
	verilogFile << "                " << "Done<=0;" << endl;
	verilogFile << "           end" << endl;
	//
	verilogFile << "else begin" << endl;
	verilogFile << "case (State)" << endl;

	// write wait state
	verilogFile << "Wait: begin" << endl;
	verilogFile <<  "         " <<"if(Start==1) begin" << endl;
	verilogFile <<  "                   " <<"State<=S0;" << endl;
	verilogFile << "                      " << "end" << endl;
	verilogFile << "         " << "else begin" << endl;
	verilogFile << "                    " << "State<=Wait;" << endl;
	verilogFile << "              " << "end" << endl;
	verilogFile << "end" << endl;

	//write each state
	for (int State = 1; State <= iVectorMax; State++){
			verilogFile << "S" << State-1<<": begin"<< endl;
			for (int iCurVertex = 1; iCurVertex <= mapCDFGSrc.size(); iCurVertex++){
				iCurStout = mapCDFGSrc.find(iCurVertex)->second.getST();
				iCurEtout = mapCDFGSrc.find(iCurVertex)->second.getET();
					if ((iCurStout<=State)&&(iCurEtout>=State)){
					iLineNumberout = mapComp.find(iCurVertex)->second.getLnNum();

					//get next/last vertex's state�� after if�� while�� else��
					
					if (strFileArr[iLineNumberout][1] == "if" || strFileArr[iLineNumberout][1] == "while" || strFileArr[iLineNumberout][1] == "else" || strFileArr[iLineNumberout][1] == "}"){
						
						//if "if" is true go to which state
						if (iCurVertex < mapComp.size()){
							iNxStVertexNum = iCurVertex + 1;
							iNxtStout = mapCDFGSrc.find(iNxStVertexNum)->second.getST();
						}
						//

						
					//	if "if" is fals go to which state
						vector<int> iSucNum = mapComp.find(iCurVertex)->second.getSucNum();
						if (iSucNum.size()){
							iNxVertexNum = iSucNum[0];
						}
						else
						{
							iNxVertexNum = 0;
						}
						if (iNxVertexNum != 0){
							iNxtEtout = mapCDFGSrc.find(iNxVertexNum)->second.getST();
						}
					//
					}
					//

					//write each line
					if (strFileArr[iLineNumberout][1] == "if") // if
						{
						verilogFile << "       " << "if";
						verilogFile << " " << strFileArr[iLineNumberout][2];
						verilogFile << " " << strFileArr[iLineNumberout][3];
						verilogFile << " " << strFileArr[iLineNumberout][4]<<endl;
						verilogFile << "     begin" << endl;
						verilogFile << "         State<=" << "S" << iNxtStout-1 << ";" << endl;
						verilogFile << "     end" << endl;
						if (iNxtStout != 0){
							verilogFile << "      else" << endl;
							verilogFile << "     begin" << endl;
							verilogFile << "         State<=" << "S" << iNxtEtout << ";" << endl;
							verilogFile << "     end" << endl;
						}
						else
						{   
							verilogFile << "      else" << endl;
							verilogFile << "     begin" << endl;
							verilogFile << "         State<=" << "Final;" << endl;
							verilogFile << "     end" << endl;
						}
						//break;
						}
					else if (strFileArr[iLineNumberout][1] == "else"){   // write else
						//verilogFile << "       " << strFileArr[iLineNumberout][1]<<endl;
						verilogFile << "         State<=" << "S" << iNxtStout-1 << ";" << endl;
						/*if (iNxtStout != 0){
							verilogFile << "         State<=" << "S" << iNxtEtout << ";" << endl;
						}
						else
						{   
							verilogFile << "         State<=" << "Final;" << endl;
						}*/
						//break;
						}
					else if (strFileArr[iLineNumberout][1] == "while"){
						verilogFile << "       " << "if";
						verilogFile << " " << strFileArr[iLineNumberout][2];
						verilogFile << " " << strFileArr[iLineNumberout][3];
						verilogFile << " " << strFileArr[iLineNumberout][4] << endl;
						verilogFile << "         State<=" << "S" << iNxtStout - 1 << ";" << endl;
						if (iNxtStout != 0){
							verilogFile << "      else" << endl;
							verilogFile << "         State<=" << "S" << iNxtEtout << ";" << endl;
						}
						else
						{
							verilogFile << "      else" << endl;
							verilogFile << "         State<=" << "Final;" << endl;
						}
					}
					else if (strFileArr[iLineNumberout][1] == "}"){
						//while go back to which state
						vector<int> iPreNum = mapComp.find(iCurVertex)->second.getPreNum();
						if (iPreNum.size()){
							iLstStVertexNum = iPreNum[0];
							iLstLineNum = mapComp.find(iLstStVertexNum)->second.getLnNum();
							if (strFileArr[iLstLineNum][1] == "while"){
								iLstStout = mapCDFGSrc.find(iLstStVertexNum)->second.getST();
								verilogFile << "         State<=" << "S" << iLstStout << ";" << endl;
							}
						}
						else
						{
							iLstStVertexNum = 0;
						}
						
						//

					}
					else{
						switch (iFileInfo[iLineNumberout]){
						case 3:
							verilogFile << "         " << strFileArr[iLineNumberout][1];
							verilogFile << "<" << strFileArr[iLineNumberout][2];
							verilogFile << strFileArr[iLineNumberout][3] << ";" << endl;
							break;
						case 5:
							verilogFile << "         " << strFileArr[iLineNumberout][1];
							verilogFile << "<" << strFileArr[iLineNumberout][2];
							verilogFile << strFileArr[iLineNumberout][3];
							verilogFile << strFileArr[iLineNumberout][4];
							verilogFile << strFileArr[iLineNumberout][5] << ";" << endl;
							break;
						case 7:
							verilogFile << "         " << strFileArr[iLineNumberout][1];
							verilogFile << "<" << strFileArr[iLineNumberout][2];
							verilogFile << strFileArr[iLineNumberout][3];
							verilogFile << strFileArr[iLineNumberout][4];
							verilogFile << strFileArr[iLineNumberout][5];
							verilogFile << strFileArr[iLineNumberout][6];
							verilogFile << strFileArr[iLineNumberout][7] << ";" << endl;
						}

						
					}

				}
			}
			//write current state to next state
			if (State != iVectorMax){
				verilogFile << "         " << "State<=" << "S" << State << ";" << endl;
			}
			else{
				verilogFile << "         " << "State<=" << "Final;" << endl;
			}
			//
			verilogFile << "    end" << endl;
	}
	//final state
	verilogFile << "Final: begin" << endl;
	verilogFile << "         " <<"Done<=1;" << endl;
	verilogFile << "         " << "State<=Wait;" << endl;
	verilogFile << "    end" << endl;
	//
	verilogFile << "endcase" << endl;
	verilogFile << "end" << endl;
	verilogFile << "end" << endl;
	verilogFile << "endmodule" << endl;
}

//-----------------------------------------------------------//
//  Sub Function Name: detect()
//  Target: Detect error
//-----------------------------------------------------------//
void detect(){
    int i, j, a, b, c, d;
    a = 0;
    b = 0;
    d = 0;
    string str[1000];
    //detect input reg error
    for (i = 1; i <=iFileInfo[0]; i++){
        if (strFileArr[i][1] == "input"){
            for (j = 1; j <= iFileInfo[i]; j++){
                if (strFileArr[i][j] == "reg" || strFileArr[i][j] == "register"){
                    cout<< " Type is wrong" << endl;
                    exit(0);
                }
            }
        }
    }
    //detect input variables' type are reg
    //save output
    for (i = 1; i <= iFileInfo[0]; i++){
        if (strFileArr[i][1] == "input"){
            for (j = 1; j <= iFileInfo[i]; j++){
                if (strFileArr[i][j] != "output"){
                    if (strFileArr[i][j] != "Int1" && strFileArr[i][j] != "Int8"&&strFileArr[i][j] != "Int16" && strFileArr[i][j] != "Int32" && strFileArr[i][j] != "Int64"&&strFileArr[i][j] != "UInt1" && strFileArr[i][j] != "UInt8"&&strFileArr[i][j] != "UInt16" && strFileArr[i][j] != "UInt32" && strFileArr[i][j] != "UInt64"){
                        str[a] = strFileArr[i][j];
                        a++;
                        b++;
                    }
                }
            }
        }
    }
    //detect
    for (i = 1; i <= iFileInfo[0]; i++){
        if ((strFileArr[i][1] == "reg") || (strFileArr[i][1] == "reg")){
            for (j = 2; j <= iFileInfo[i]; j++){
                d = 0;
                if (strFileArr[i][j] != "Int1" && strFileArr[i][j] != "Int8"&&strFileArr[i][j] != "Int16" && strFileArr[i][j] != "Int32" && strFileArr[i][j] != "Int64"&&strFileArr[i][j] != "UInt1" && strFileArr[i][j] != "UInt8"&&strFileArr[i][j] != "UInt16" && strFileArr[i][j] != "UInt32" && strFileArr[i][j] != "UInt64"){
                    for (c = 0; c < b; c++){
                        if (strFileArr[i][j] == str[c]){
                            d = 1;
                        }
                    }
                    if (d == 1){
                        cout << strFileArr[i][j] << " Type is wrong" << endl;
                        exit(0);
                    }
                }
            }
        }
    }
}
