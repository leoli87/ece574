`timescale 1ns / 1ps
module MOD(a,b,rem);
	 #（parameter DATAWIDTH=64）
    input[(DATAWIDTH-1):0] a,b;
    output reg [(DATAWIDTH-1):0] rem;
	 always@(a,b) begin
		rem<=a%b; 
	 end
endmodule
