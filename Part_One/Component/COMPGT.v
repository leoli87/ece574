`timescale 1ns / 1ps
module COMPGT(a,b,gt);
    #（parameter DATAWIDTH=64）
    input [(DATAWIDTH-1):0] a,b;
    output reg gt;
	 always @(a,b)begin
		if(a>b)begin
				gt<=1;
		end
		else
		   begin
				gt<=0;
			end
	 end
endmodule
