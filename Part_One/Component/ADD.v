`timescale 1ns / 1ps
module ADD(a,b,sum);
    #（parameter DATAWIDTH=64）
    input [(DATAWIDTH-1):0] a,b;
    output reg [(4*DATAWIDTH-1):0] sum;
	 always@(a,b)begin
	 sum<=a+b;
	 end 
endmodule
