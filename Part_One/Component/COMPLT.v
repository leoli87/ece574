`timescale 1ns / 1ps
module COMPLT(a,b,lt);
    #（parameter DATAWIDTH=64）
    input [(DATAWIDTH-1):0] a;
    input [(DATAWIDTH-1):0] b;
    output reg lt;

	 always @(a, b)begin
		if(a<b)begin
				lt<=1;
		end
		else
		   begin
				lt <= 0;
			end
			
	 end
endmodule
