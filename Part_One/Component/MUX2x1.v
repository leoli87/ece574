`timescale 1ns / 1ps
module MUX2x1(a,b,sel,d);
	 #（parameter DATAWIDTH=64）
    input [(DATAWIDTH-1):0] a,b;
    input sel;
    output reg [(DATAWIDTH-1):0] d;
	 always@(a,b,sel)begin
		d=sel ? a:b;
	 end
endmodule
