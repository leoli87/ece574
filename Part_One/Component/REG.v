`timescale 1ns / 1ps
module REG(d,clk,rst,q);
	 #（parameter DATAWIDTH=64）
    input [(DATAWIDTH-1):0]d;
    input  clk;
    input  rst;
    output reg[(DATAWIDTH-1):0]q;
	 always @(posedge clk) begin
	 if (rst==1)
		q<=d;
	 else
		q<=0;
	 end
endmodule
