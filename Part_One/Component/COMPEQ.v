`timescale 1ns / 1ps
module COMPEQ(a,b,eq);
    #（parameter DATAWIDTH=64）
    input [(DATAWIDTH-1):0] a,b;
    output reg eq;
	 always @(a,b)begin
		if(a==b)begin
			eq <= 1;
		end
		else 
		   begin
				eq <= 0;
			end
	 end
endmodule
