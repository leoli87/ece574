`timescale 1ns / 1ps
module circuit_7(a,b,c,d,zero,z,clk,rst);
	input [63:0] a, b, c, d, zero;
	output [63:0] z;
	input clk, rst;
	wire [63:0] e, f, g, zwire;	
	wire  gEQz;  

	DIV #(.DATAWIDTH(16)) DIV_0(a,b,e);							//e = a / b
	DIV #(.DATAWIDTH(16)) DIV_1(c,d,f);							//f = c / d
	MOD #(.DATAWIDTH(16)) MOD_0(a,b,g);							//g = a % b
	COMPEQ #(.DATAWIDTH(16)) COMPEQ_0(g,zero,gEQz);			//gEQz = g == zero
	MUX2x1 #(.DATAWIDTH(16)) MUX2x1_0(gEQz,e,f,zwire);		//zwire = gEQz ? e : f
	REG #(.DATAWIDTH(16)) REG_0(zwire,clk,rst,z);
endmodule
