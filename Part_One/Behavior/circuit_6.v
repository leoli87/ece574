`timescale 1ns / 1ps
module circuit_6(a,b,c,d,e,f,g,h,clk,rst,num,avg);
	input [15:0] a, b, c, d, e, f, g, h, num;
	input clk,rst;
	output [15:0] avg;
	wire [15:0] r1, r2, r3, r4, r5, r6, r7; 
	wire [15:0] avgwire;
	wire [31:0] t1, t2, t3, t4, t5, t6, t7;

	ADD #(.DATAWIDTH(16)) ADD_0(a,b,t1);				//t1 = a + b
	REG #(.DATAWIDTH(16)) REG_0(t1,clk,rst,r1);		//r1 = t1
	ADD #(.DATAWIDTH(16)) ADD_1(r1,c,t2);				//t2 = r1 + c
	REG #(.DATAWIDTH(16)) REG_1(t2,clk,rst,r2);		//r2 = t2
	ADD #(.DATAWIDTH(16)) ADD_2(r2,d,t3);				//t3 = r2 + d
	REG #(.DATAWIDTH(16)) REG_2(t3,clk,rst,r3);		//r3 = t3
	ADD #(.DATAWIDTH(16)) ADD_3(e,r3,t4);				//t4 = r3 + e
	REG #(.DATAWIDTH(16)) REG_3(t4,clk,rst,r4);		//r4 = t4
	ADD #(.DATAWIDTH(16)) ADD_4(f,r4,t5);				//t5 = r4 + f
	REG #(.DATAWIDTH(16)) REG_4(t5,clk,rst,r5);		//r5 = t5
	ADD #(.DATAWIDTH(16)) ADD_5(g,r5,t6);				//t6 = r5 + g
	REG #(.DATAWIDTH(16)) REG_5(t6,clk,rst,r6);		//r6 = t6
	ADD #(.DATAWIDTH(16)) ADD_6(h,r6,t7);				//t7 = r6 + h
	REG #(.DATAWIDTH(16)) REG_6(t7,clk,rst,r7);		//r7 = t7
	DIV #(.DATAWIDTH(16)) DIV_0(r7,num,avgwire);		//avgwire = r7 / num
	REG #(.DATAWIDTH(16)) REG_7(avgwire,clk,rst,avg);
endmodule
