`timescale 1ns / 1ps
module circuit_2(a, b, c, x, z, clk, rst);
	input [31:0] a, b, c;
	input clk, rst;
	output [31:0]  z, x;
	wire [31:0]  d, e, f, g, h;
	wire dLTe, dEQe;  
	wire [31:0] zwire, xwire;


	ADD #(.DATAWIDTH(16)) ADD_0(a, b, d);					//d = a + b
	ADD #(.DATAWIDTH(16)) ADD_1(a, c, e);					//e = a + c
	SUB #(.DATAWIDTH(16)) SUB_0(a, b, f);					//f = a - b
	COMPEQ #(.DATAWIDTH(16)) COMPEQ_0(d, e, dEQe);			//dEQe = d == e
	COMPLT #(.DATAWIDTH(16)) COMPLT_0(d, e, dLTe);			//dLTe = d < e
	MUX2x1 #(.DATAWIDTH(16)) MUX2x1_0(d, e, dLTe, g);		//g = dLTe ? d : e
	MUX2x1 #(.DATAWIDTH(16))  MUX2x1_1(g, f, dEQe, h);		//h = dEQe ? g : f
	SHL #(.DATAWIDTH(16)) SHL_0(h, dLTe, xwire);				//xwire = h << dLTe
	SHL #(.DATAWIDTH(16)) SHL_1(h, dEQe, zwire);				//zwire = h >> dEQe
	REG #(.DATAWIDTH(16)) REG_0(xwire, clk, rst, x);		//x = xwire
	REG #(.DATAWIDTH(16)) REG_1(zwire, clk, rst, z);		//z = zwire


endmodule
