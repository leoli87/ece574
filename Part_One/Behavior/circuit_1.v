`timescale 1ns / 1ps
module circuit_1(a, b, c, x, z, clk, rst);
	input [7:0] a, b, c;
	input clk, rst;
	output [7:0] z;
	output [15:0] x;
	wire [7:0] d, e;
	wire [15:0] f;
	wire [15:0] xwire;
	wire [7:0] zwire;
	wire g;

		ADD #(.DATAWIDTH(16)) ADD_0(a, b, d);
		ADD #(.DATAWIDTH(16)) ADD_1(a, c, e);
		COMPLT #(.DATAWIDTH(16)) COMPLT_0(d, e, g);
		MUX2x1 #(.DATAWIDTH(16)) MUX2x1_0(d, e, g, zwire);
			 
		MUL #(.DATAWIDTH(16)) MUL_0(a, c, f);
		SUB #(.DATAWIDTH(16)) SUB_0(f, d, xwire);
		
		REG #(.DATAWIDTH(16)) REG_0(xwire, clk, rst, x);
		REG #(.DATAWIDTH(16)) REG_1(zwire, clk, rst, z);


endmodule
