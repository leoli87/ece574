`timescale 1ns / 1ps
module circuit_4(a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p,clk, rst,final);
	input [7:0] a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p;	
   input clk, rst;
	output [31:0] final;
	wire [31:0] t1, t2, t3, t4, t5, t6, t7, t8, t9, t10, t11, t12, t13, t14;
	
	ADD #(.DATAWIDTH(16)) ADD_0(a,b,t1);//t1 = a + b
	ADD #(.DATAWIDTH(16)) ADD_1(t1,c,t2);//t2 = t1 + c
	ADD #(.DATAWIDTH(16)) ADD_2(t2,d,t3);//t3 = t2 + d
	ADD #(.DATAWIDTH(16)) ADD_3(t3,e,t4);//t4 = t3 + e
	ADD #(.DATAWIDTH(16)) ADD_4(t4,f,t5);//t5 = t4 + f
	ADD #(.DATAWIDTH(16)) ADD_5(t5,g,t6);//t6 = t5 + g
	ADD #(.DATAWIDTH(16)) ADD_6(t6,h,t7);//t7 = t6 + h
	ADD #(.DATAWIDTH(16)) ADD_7(t7,i,t8);//t8 = t7 + i
	ADD #(.DATAWIDTH(16)) ADD_8(t8,j,t9);//t9 = t8 + j
	ADD #(.DATAWIDTH(16)) ADD_9(t9,l,t10);//t10 = t9 + l
	ADD #(.DATAWIDTH(16)) ADD_10(t10,m,t11);//t11 = t10 + m
	ADD #(.DATAWIDTH(16)) ADD_11(t11,n,t12);//t12 = t11 + n
	ADD #(.DATAWIDTH(16)) ADD_12(t12,o,t13);//t13 = t12 + o
	ADD #(.DATAWIDTH(16)) ADD_13(t13,p,t14);//t14 = t13 + p
	
	REG REG_0(t14, clk, rst, final);

endmodule
