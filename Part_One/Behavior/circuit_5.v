`timescale 1ns / 1ps
module circuit_5(a,b,c,z,x,clk,rst);	
	input [63:0] a, b, c;
	input  clk,rst;
	output [31:0] z, x;
	wire [63:0] d, e, f, g, h;
	wire  dLTe, dEQe;
	wire [63:0] xrin, zrin;
	wire [63:0] greg, hreg;

	ADD #(.DATAWIDTH(16)) ADD_0(a,b,d);						//d = a + b
	ADD #(.DATAWIDTH(16)) ADD_1(a,c,e);						//e = a + c
	SUB #(.DATAWIDTH(16)) SUB_0(a,b,f);						//f = a - b
	COMPEQ #(.DATAWIDTH(16)) COMPEQ_0(d,e,dEQe);			//dEQe = d == e
	COMPLT #(.DATAWIDTH(16)) COMPLT_0(d,e,dLTe);			//dLTe = d < e
	MUX2x1 #(.DATAWIDTH(16)) MUX2x1_0(d,e,dLTe,g);		//g = dLTe ? d : e
	MUX2x1 #(.DATAWIDTH(16)) MUX2x1_1(g,f,dEQe,h);		//h = dEQe ? g : f
	REG #(.DATAWIDTH(16)) REG_0(g,clk,rst,greg);			//greg = g
	REG #(.DATAWIDTH(16)) REG_1(h,clk,rst,hreg);			//hreg = h
	SHL #(.DATAWIDTH(16)) SHL_0(hreg,dLTe,xrin);			//xrin = hreg << dLTe
	SHR #(.DATAWIDTH(16)) SHR_0(greg,dEQe,zrin);			//zrin = greg >> dEQe
	REG #(.DATAWIDTH(16)) REG_2(xrin, clk, rst, x);
	REG #(.DATAWIDTH(16)) REG_3(zrin, clk, rst, z);
	
endmodule
