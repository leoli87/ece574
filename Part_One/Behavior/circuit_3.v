`timescale 1ns / 1ps
module circuit_3(a, b, c, d, e, f, g, h, sa, clk, rst, avg);
	input [15:0] a, b, c, d, e, f, g, h;
	input [7:0] sa;
	input clk, rst;	
	output [15:0] avg;
	wire [31:0] l00, l01, l02, l03, l10, l11, l2, l2div2, l2div4, l2div8;

	ADD #(.DATAWIDTH(16)) ADD_0(a, b, l00);								//l00 = a + b
	ADD #(.DATAWIDTH(16)) ADD_1(c, d, l01);								//l01 = c + d
	ADD #(.DATAWIDTH(16)) ADD_2(e, f, l02);								//l02 = e + f
	ADD #(.DATAWIDTH(16)) ADD_3(g, h, l03);								//l03 = g + h
	ADD #(.DATAWIDTH(16)) ADD_4(l00, l01, l10);							//l10 = l00 + l01
	ADD #(.DATAWIDTH(16)) ADD_5(l02, l03, l11);							//l11 = l02 + l03
	ADD #(.DATAWIDTH(16)) ADD_6(l10, l11, l2);							//l2 = l10 + l11
	SHR #(.DATAWIDTH(16)) SHR_0(l2, sa, l2div2);							//l2div2 = l2 >> sa
	SHR #(.DATAWIDTH(16)) SHR_1(l2div2, sa, l2div4);					//l2div4 = l2div2 >> sa
	SHR #(.DATAWIDTH(16)) SHR_2(l2div4, sa, l2div8);					//l2div8 = l2div4 >> sa
	REG #(.DATAWIDTH(16)) REG_0(l2div8, clk, rst, avg);				//avg = l2div8
	
endmodule
